<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 6/28/18 9:02 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionQuizzesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_quizzes', function (Blueprint $table) {
            $table->increments('id');
            $table->text('header')->comment('Encabezado de la pregunta.');
            $table->text('body')->comment('Cuerpo de la pregunta.');
            $table->integer('sequence_number')->default('0')->comment('Orden en el que se mostraran las preguntas.');
            $table->integer('quiz_master')->unsigned()->index()->comment('La relación al test.');
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('quiz_master')->references('id')->on('quizzes')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pregunta_quizzes');
    }
}
