<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 6/28/18 9:14 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDegreeInstitutionEnclosuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('degree_institution_enclosures', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('degree_institution')->unsigned()->index()->comment('En que recintos se encuentra disponible esta carrera');
            $table->integer('enclosure')->unsigned()->index()->comment('enclosure');
            $table->timestamps();

            $table->foreign('degree_institution')->references('id')->on('degrees')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('enclosure')->references('id')->on('enclosures')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('degree_institution_enclosures');
    }
}
