<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 6/28/18 9:02 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCataloguesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalogues', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 50)->index()->unique()->nullable();
            $table->string('name', 500);
            $table->longText('description')->nullable();
            $table->integer('master')->unsigned()->index()->nullable()->comment('Relación inclusiva, puede tener hijos');
            $table->integer('icon')->unsigned()->index()->nullable()->comment('Poder relacionar un icono al catalogo');
            $table->foreign('master')->references('id')->on('catalogues')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('icon')->references('id')->on('icon_resources');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catalogues');
    }
}
