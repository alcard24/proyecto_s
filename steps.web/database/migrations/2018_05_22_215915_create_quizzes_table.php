<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 6/28/18 9:02 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuizzesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quizzes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 500)->comment('Nombre del test.');
            $table->text('propose')->comment('Proposito del test');
            $table->integer('test_type')->unsigned()->index()->comment('Tipo de test ("Respuesta unica, multi respuesta, mixto")');
            $table->integer('author')->unsigned()->index()->comment('');
            $table->boolean('shared')->default(false)->comment('Compartido, con la comunidad de info-edu para poder ser realizado en cualquier momento');
            $table->boolean('order_by_sequence')->default('true')->comment('Comportamiento, donde las preguntas se ordenaran según el número de secuencia.');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('test_type')->references('id')->on('catalogues')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('author')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quizzes');
    }
}
