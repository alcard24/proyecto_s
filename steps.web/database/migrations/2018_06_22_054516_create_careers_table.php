<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 6/28/18 9:02 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCareersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('careers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('especial_of')->unsigned()->index()->nullable()->comment('Relación para identificarlo como una especialidad de una carrera.');
            $table->integer('table_content')->unsigned()->index()->nullable()->comment('Relación para generar la tabla de contenidos, que describe la carrera.');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('especial_of')->references('id')->on('careers')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('table_content')->references('id')->on('content_parents')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carrers');
    }
}
