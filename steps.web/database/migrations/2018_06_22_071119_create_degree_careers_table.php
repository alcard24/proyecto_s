<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 6/28/18 9:13 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDegreeCareersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('degree_careers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('career')->unsigned()->index();
            $table->integer('degree')->unsigned()->index()->comment('Tabla puente');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('career')->references('id')->on('careers')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('degree')->references('id')->on('degrees')
                ->onUpdate('cascade')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('degree_carrers');
    }
}
