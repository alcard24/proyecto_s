<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 6/28/18 9:13 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDegreeInstitutionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('degree_institutions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('degree')->unsigned()->index();
            $table->integer('content')->unsigned()->index()->nullable()->comment('infromacion extra, de la carrera segun el colegio/ instituto. Ejemplo. el plan de clases');
            $table->integer('institution')->unsigned()->index();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('degree')->references('id')->on('degrees')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('content')->references('id')->on('content_parents')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('institution')->references('id')->on('institutions')
                ->onUpdate('cascade')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('degree_institutions');
    }
}
