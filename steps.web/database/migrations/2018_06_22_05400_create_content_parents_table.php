<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 6/28/18 9:02 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentParentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_parents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 500)->default('Tabla de contenido')->comment('Titulo de la tabla de contenido.');
            $table->text('description')->nullable()->comment('Descripcion de la tabla de contenido.');
            $table->boolean('display_before_default')->default(false)->comment('Renderizar después de la  tabla de contenido por defecto.');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_parents');
    }
}
