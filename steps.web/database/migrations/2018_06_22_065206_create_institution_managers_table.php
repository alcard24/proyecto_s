<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 6/28/18 9:03 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstitutionManagersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('institution_managers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user')->unsigned()->index();
            $table->integer('institution')->unsigned()->index();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('institution')->references('id')->on('institutions')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('institution_managers');
    }
}
