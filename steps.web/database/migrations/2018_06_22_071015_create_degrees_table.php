<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 6/28/18 9:03 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDegreesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('degrees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 500)->comment('Nombre de la carrera.');
            $table->text('description')->nullable();
            $table->integer('title_won')->unsigned()->index()->comment('Titulo ganado "Bachiller, Ingeniero, etc..."');
            $table->integer('min_education_level')->unsigned()->index()->nullable()->comment('Educacion minima requerida, para estudiar el titulo');
            $table->double('time_to_complete')->nullable()->comment('Tiempo de duración del estudio.');
            $table->integer('time_measure')->unsigned()->nullable()->index()->comment('Medida de tiempo');
            $table->double('time_in_years')->nullable()->index()->comment('Tiempo en años para poder hacer filtros.');
            $table->integer('contents')->nullable()->unsigned()->index()->comment('Puede añadir otro contenido, para describir mejor. la carrera');
            $table->integer('faculty')->nullable()->unsigned()->index()->comment('Facultad, o tipo de estudio ejemplo "Ciencias exactas.".');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('title_won')->references('id')->on('catalogues')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('min_education_level')->references('id')->on('catalogues')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('time_measure')->references('id')->on('catalogues')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('contents')->references('id')->on('content_parents')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('faculty')->references('id')->on('catalogues')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('degrees');
    }
}
