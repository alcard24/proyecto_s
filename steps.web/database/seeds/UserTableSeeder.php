<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 7/1/18 4:29 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

use App\Steps\Role\Role;
use App\Steps\User\User;
use Illuminate\Database\Seeder;

/**
 * Class User Table Seeder
 */
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $owner = Role::findOrFail(1);
        $admin = Role::findOrFail(2);
        //Seed the default users for the admin
        $juan = new User;
        $juan->email = 'juan_ubau@hotmail.com';
        $juan->password = bcrypt('qwertyuiop123');
        $juan->name = 'Juan Guillermo Ubau Gonzalez';
        $juan->save();
        $juan->attachRole($admin);

        $fred = new User;
        $fred->email = 'freddy.gomez.245@gmail.com';
        $fred->password = bcrypt('a_developer');
        $fred->name = 'Frederick Amilcar Valle Gomez';
        $fred->save();
        $fred->attachRole($admin);

        $fender = new User();
        $fender->email = 'fenmora@outlook.es';
        $fender->password = bcrypt('a_developer');
        $fender->name = 'Fender Josue Mora Calero';
        $fender->save();
        $fender->attachRole($admin);

        $alex = new User;
        $alex->email = 'kevinnica02@hotmail.com';
        $alex->password = bcrypt('qwertyForDev');
        $alex->name = 'Kevin Alexander Gaitan Arguello';
        $alex->save();
        $alex->attachRole($owner);
        $alex->attachRole($admin);

    }
}
