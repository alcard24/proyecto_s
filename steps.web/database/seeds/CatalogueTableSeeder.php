<?php

use App\Steps\Catalogue\Catalogue;
use Illuminate\Database\Seeder;

class CatalogueTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Insert all default test type

        $testType = new Catalogue();

        $testType->name = 'Test Types';
        $testType->description = 'All Test Type Allowed';
        $testType->code = 'TT';
        $testType->icon = 91;
        $testType->save();


        $multipleAnswer = new Catalogue();

        $multipleAnswer->name = 'Multi Answer';
        $multipleAnswer->description = 'Allow One or more answer for a question';
        $multipleAnswer->code = 'TT-01';
        $multipleAnswer->icon = 157;
        $multipleAnswer->master = $testType->id;
        $multipleAnswer->save();

        $uniqueAnswer = new Catalogue();

        $uniqueAnswer->name = 'Unique Answer';
        $uniqueAnswer->description = 'Allow Only One Answer for a question';
        $uniqueAnswer->code = 'TT-02';
        $uniqueAnswer->icon = 670;
        $uniqueAnswer->master = $testType->id;
        $uniqueAnswer->save();

    }
}
