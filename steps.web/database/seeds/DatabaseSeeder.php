<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 7/1/18 4:36 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

use Illuminate\Database\Seeder;

/**
 * Class Database Seeder
 */
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(IconResourceTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(CatalogueTableSeeder::class);
    }
}
