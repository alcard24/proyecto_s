<?php

/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 6/28/18 9:01 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */


/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Steps\User\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\Enclosure::class, function (Faker\Generator $faker) {
    return [
        'nombre' => $faker->company,
        'dirreccion' => $faker->address,
        'latitud' => $faker->latitude,
        'longitud' => $faker->longitude,
        'web' => $faker->url,
        'institucion' => function () {
            return factory('App\Institution');
        }
    ];
});

$factory->define(App\Institution::class, function (Faker\Generator $faker) {
    return [
        'nombre' => $faker->company,
        'webpage' => $faker->url,
        'path_logo' => $faker->imageUrl(),
        'path_background' => $faker->imageUrl(),
        'mision' => $faker->paragraph,
        'vision' => $faker->paragraph
    ];
});

$factory->define('App\NivelAcademico', function (Faker\Generator $faker) {
    return [
        'nombre' => $faker->colorName,
        'descripcion' => $faker->paragraph,
        'sortOrder' => $faker->numberBetween(0, 10)
    ];
});
