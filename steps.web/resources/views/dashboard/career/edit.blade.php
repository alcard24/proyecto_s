@extends('layouts.dashboard')
@section('content')
    @if(isset($career))
        {{ Breadcrumbs::render('career.edit',$career) }}
    @else
        {{ Breadcrumbs::render('career.create') }}
    @endif

    <div class="header text-center">
        <h3 class="title">
            @if(isset($career))
                Modificando {{$career->name}} [{{$career->id}}]
            @else
                Agregar una nueva carrera
            @endif
        </h3>
    </div>

    <div class="col-md-12">
        <!-- Kavv -->
        <!-- Added a tag with id "mensaje" for use with the tiposmensajes script -->
        <div id="mensaje"></div>

        <div class="card">
            <div class="card-header">

            </div>
            <div class="card-content">
                @if(isset($career))
                {!! Form::model($career, ['url' => ['dashboard/user', $career->id], 'method' => 'patch', 'id' => 'data'])  !!}
                @else
                <!-- Kavv -->
                <!-- changed the tag because it wasn't necessary -->
                <form id="data">
                @endif
                    <!-- Kavv -->
                    <!-- Added tag for token -->
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group label-floating @if(!isset($career)) is-empty @endif">
                                <label for="name" class="control-label">Name</label>
                                <input name="name" type="text"
                                    @if(isset($career))
                                    value="{{$career->name}}"
                                    @endif
                                    class="form-control material-input">
                            </div>
                        </div>

                        <div class="col-md-6">
                            @include('dashboard.career.select')
                        </div>
                        
                        <div class="col-md-12">
                            <div class="form-group label-floating  @if(!isset($career)) is-empty @endif">
                                <label for="content" class="control-label">Content</label>
                                <input name="content" type="text" 
                                    @if(isset($career))
                                    value="{{$career->table_content}}"
                                    @endif
                                    class="form-control material-input">
                            </div>

                        </div>
                    </div>        
                    <!-- Kavv -->
                    <!-- Condition to select the button acctions -->
                    @if(isset($career))
                    <button class="btn btn-success" type="button" onclick="Update();">
                    @else
                    <button class="btn btn-success" type="button" onclick="Save()">
                    @endif
                        Guardar
                        <div class="ripple-container"></div>
                    </button>
                {{ Form::close()}}
            </div>
        </div>
    </div>
@stop
@section('javascript')
    <!-- Include Editor style. -->
    <link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.7.6/css/froala_editor.min.css'
          rel='stylesheet' type='text/css'/>

    <!-- Include JS file. -->
    <script type='text/javascript'
            src='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.7.6/js/froala_editor.min.js'></script>
    
    <!-- Mensaje -->
    <script src="/js/kavv/tiposmensajes.js"></script>
    
    <!-- Initialize the editor. -->
    <script>
        $(document).ready(function () {

        });

        @if(isset($career))
        //Update user
        function Update()
        {
            return $.ajax({
                url: '{{route('career.update',$career->id)}}',
                headers: {'X-CSRF-TOKEN': $("#token").val()},
                type: 'PUT',
                dataType: 'json',
                data: $("#data").serialize(),
                success: function(res){
                    if(res.action === 1)
                        //message([Message Array], {manual:bool,objeto:$(#),tipo:'danger,info,success, etc'})
                        message(res.message, {manual:true, tipo:'danger'});                  
                    else
                        location.href = "/dashboard/career";
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                message(jqXHR,{tipo:"danger"});
            });
        }
        @else 
        //Save User
        function Save()
        {
            return $.ajax({
                url: '{{route('career.store')}}',
                headers: {'X-CSRF-TOKEN': $("#token").val()},
                type: 'POST',
                dataType: 'json',
                data: $("#data").serialize(),
                success: function(res){
                    if(res.action === 1)
                        message(res.message, {manual:true, tipo:'danger'});                    
                    else
                        location.href = "/dashboard/career";
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                message(jqXHR,{tipo:"danger"});
            });
        }
        @endif
    </script>

    @parent
@stop
