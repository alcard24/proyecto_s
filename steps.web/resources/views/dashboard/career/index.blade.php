@extends('layouts.dashboard')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{ Breadcrumbs::render('career.index') }}

    <div class="header text-center">
        <h3 class="title">Carreras registradas en el sistema.</h3>
        <p class="category">lista de carreras</p>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-content">
                    {{--@include('dashboard.partials.search')--}}
                    <div class="toolbar" style="display: inline-block; width: 100%;">
                        <div class="pull-left">
                            <div class="btn-group pull-left" style="margin-right: 10px">
                                <a href="{{action('CareerController@create')}}">
                                    <button class="btn btn-sm btn-success">
                                        <i class="material-icons">save</i>&nbsp;&nbsp;Nuevo
                                    </button>
                                </a>
                            </div>
                        </div>
                        <div class="pull-right">
                            <div class="btn-group pull-right" style="margin-right: 10px">
                                <button class="btn btn-sm reload btn-facebook btn-raised">
                                    <i class="material-icons">replay</i>&nbsp;&nbsp;Recargar
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="material-datatables">
                        <table id="datatables" class="table table-striped table-no-bordered table-hover"
                               cellspacing="0"
                               width="100%" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Special Of</th>
                                    <th>Content</th>
                                    <th>Created at</th>
                                    <th class="text-left">Options</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                     aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="btn btn-simple close" data-dismiss="modal"
                                        aria-hidden="true">
                                    <i class="material-icons">clear</i>
                                </button>
                                <h4 class="modal-title"></h4>
                            </div>
                            <div class="modal-body">
                                <p class="description-modal">Esta seguro ?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-simple" data-dismiss="modal">Cancelar</button>
                                <button type="button" class="btn btn-danger btn-simple delete" data-dismiss="modal">
                                    Sí
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('javascript')

    <script type="text/javascript">
    
        var table, theValue, row;

        $(document).ready(function () {
            table = $('#datatables').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{route('career.indexAjax')}}',
                columns: [
                    {data: 'name', name: 'name'},
                    {data: 'specialPosition', name: 'specialPosition'},
                    {data: 'content', name: 'content'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
                columnDefs: [
                    {className: "td-actions text-right", "targets": [4]}
                ],
                "lengthMenu": [
                    [10, 25, 50],
                    [10, 25, 50]
                ],
                responsive: true,
                language: {
                    search: "_INPUT_",
                    searchPlaceholder: "Search records",
                    lengthMenu: "Ver _MENU_ por página",
                    zeroRecords: "No se encontraron coincidencias - :c",
                    info: "Página _PAGE_ de _PAGES_",
                    infoEmpty: "No hay datos",
                    paginate: {
                        first: "Primero",
                        last: "Último",
                        next: "Siguiente",
                        previous: "Anterior"
                    },
                    sProcessing: "<div id=\"contenedor\"><div class=\"loader\" id=\"loader\">Loading...</div></div>"
                }

            });
        });
        
        $(document).on('click', '.remove', function () {
            $('.description-modal').text("Esta seguro que desea eliminar este registro");
            $('#myModal').modal('show');
            theValue = $(this).data('info');
            row = $(this).parents('tr');
            console.log($(this).parents('tr'));
        });
        $(document).on('click', '.delete', function (e) {
            $.ajax(
                {
                    type: "DELETE",
                    url: '/dashboard/career/' + theValue,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType: 'json',
                    success: function (res) {
                        table.row(row).remove().draw( false );
                    }
                }
            );
        });
        $(document).on('click', '.reload', function (e) {
            location.reload();
        });
    </script>
    @parent
@stop