@extends('layouts.dashboard')
@section('content')
    @if(isset($usuario))
        {{ Breadcrumbs::render('user.edit',$usuario) }}
    @else
        {{ Breadcrumbs::render('user.create') }}
    @endif

    <div class="header text-center">
        <h3 class="title">
            @if(isset($usuario))
                Modificando {{$usuario->name}} [{{$usuario->id}}]
            @else
                Agregar un nuevo usuario
            @endif
        </h3>
    </div>

    <div class="col-md-12">
        <!-- Kavv -->
        <!-- Added a tag with id "mensaje" for use with the tiposmensajes script -->
        <div id="mensaje"></div>
        <!-- Kavv -->
        <!-- this is useless now -->
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="card">
            <div class="card-header">
            <!-- Kavv -->
            <!-- When u create an user, u don't must show the togglebutton -->
                @if(isset($usuario))
                    <div class="togglebutton">
                        <label>
                            <input type="checkbox" id="canReset" name="can_reset" onchange="hidePassword()">
                            Volver a establecer contraseña
                        </label>
                    </div>
                @endif
            </div>
            <div class="card-content">
                @if(isset($usuario))
                {!! Form::model($usuario, ['url' => ['dashboard/user', $usuario->id], 'method' => 'patch', 'id' => 'data'])  !!}
                @else
                <!-- Kavv -->
                <!-- changed the tag because it wasn't necessary -->
                <form id="data">
                @endif
                    <!-- Kavv -->
                    <!-- Added tags for token and one more for the togle button state  -->
                    <input id="changePassword" type="hidden" name="changePassword" value="0">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group label-floating @if(!isset($usuario)) is-empty @endif">
                                <label for="name" class="control-label">Nombre del usuario</label>
                                <input name="name" type="text"
                                    @if(isset($usuario))
                                    value="{{$usuario->name}}"
                                    @endif
                                    class="form-control material-input">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group label-floating @if(!isset($usuario)) is-empty @endif">
                                <label for="email" class="control-label">Email</label>
                                <input name="email" type="email"
                                    @if(isset($usuario))
                                    value="{{$usuario->email}}"
                                    @endif
                                    class="form-control material-input">
                            </div>
                        </div>
                        <div class="col-md-6" id="divPassword" @if(isset($usuario))hidden
                            style="visibility: hidden; display: none;" @endif>
                            <div class="form-group label-floating  @if(!isset($usuario)) is-empty @endif">
                                <label for="password" class="control-label">Contraseña del usuario</label>
                                <div class="input-group">
                                    <input name="password" type="password"
                                        class="form-control material-input">
                                    <span class="input-group-btn">
                                        <button class="btn btn-round btn-primary" id="limpiar" data-toggle="tooltip" data-placement="top"
                                                title="Limpiar">
                                            <i class="material-icons">clear</i>
                                        </button>

                                        <button class="btn btn-round btn-primary" id="generar_password" data-toggle="tooltip" data-placement="top"
                                                title="Generar contraseña">
                                            <i class="material-icons">settings_backup_restore</i>
                                        </button>
                                    </span>
                                </div>
                            </div>

                        </div>
                    </div>        
                    <!-- Kavv -->
                    <!-- Condition to select the button acctions -->
                    @if(isset($usuario))
                    <button class="btn btn-success" type="button" onclick="Update();">
                    @else
                    <button class="btn btn-success" type="button" onclick="Save()">
                    @endif
                        Guardar
                        <div class="ripple-container"></div>
                    </button>
                {{ Form::close()}}
            </div>
        </div>
    </div>
@stop
@section('javascript')
    <!-- Include Editor style. -->
    <link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.7.6/css/froala_editor.min.css'
          rel='stylesheet' type='text/css'/>

    <!-- Include JS file. -->
    <script type='text/javascript'
            src='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.7.6/js/froala_editor.min.js'></script>
    
    <!-- Mensaje -->
    <script src="/js/kavv/tiposmensajes.js"></script>
    
    <!-- Initialize the editor. -->
    <script>
        $(document).ready(function () {
            $('#limpiar').on('click', function (event) {
                event.preventDefault();
            });

            $('#limpiar').on('click', function (event) {
                event.preventDefault();
            });
        });

        function hidePassword() {
            //Kavv
            //only to follow the sequence but it is not necessary that it be established as invisible
            if($('#canReset').prop('checked'))
            {
                //ON
                $("#divPassword").css("visibility","visible");
                $("#divPassword").show()
                $('#changePassword').val(1);
            }
            else
            {
                //OFF
                $("#divPassword").css("visibility","hidden");
                $("#divPassword").hide()
                $('#changePassword').val(0);
            }
        }
        function generatePassword() {
            var length = 8,
                charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
                retVal = "";
            for (var i = 0, n = charset.length; i < length; ++i) {
                retVal += charset.charAt(Math.floor(Math.random() * n));
            }
            return retVal;
        }
        
        @if(isset($usuario))
        //Update user
        function Update()
        {
            return $.ajax({
                url: '{{route('user.update',$usuario->id)}}',
                headers: {'X-CSRF-TOKEN': $("#token").val()},
                type: 'PUT',
                dataType: 'json',
                data: $("#data").serialize(),
                success: function(res){
                    if(res.action === 1)
                        //message([Message Array], {manual:bool,objeto:$(#),tipo:'danger,info,success, etc'})
                        message(res.message, {manual:true, tipo:'danger'});                  
                    else
                        location.href = "/dashboard/user";
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                message(jqXHR,{tipo:"danger"});
            });
        }
        @else 
        //Save User
        function Save()
        {
            return $.ajax({
                url: '{{route('user.store')}}',
                headers: {'X-CSRF-TOKEN': $("#token").val()},
                type: 'POST',
                dataType: 'json',
                data: $("#data").serialize(),
                success: function(res){
                    if(res.action === 1)
                        message(res.message, {manual:true, tipo:'danger'});                    
                    else
                        location.href = "/dashboard/user";
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                message(jqXHR,{tipo:"danger"});
            });
        }
        @endif
    </script>

    @parent
@stop
