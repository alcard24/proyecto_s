@extends('layouts.dashboard')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{ Breadcrumbs::render('user.index') }}

    <div class="header text-center">
        <h3 class="title">Usuarios registrados en el sistema.</h3>
        <p class="category">lista de usuarios</p>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-content">
                    {{--@include('dashboard.partials.search')--}}
                    <div class="toolbar" style="display: inline-block; width: 100%;">
                        <div class="pull-left">
                            <div class="btn-group pull-left" style="margin-right: 10px">
                                <a href="{{action('UserController@create')}}">
                                    <button class="btn btn-sm btn-success">
                                        <i class="material-icons">save</i>&nbsp;&nbsp;Nuevo
                                    </button>
                                </a>
                            </div>
                        </div>
                        <div class="pull-right">
                            <div class="btn-group pull-right" style="margin-right: 10px">
                                <button class="btn btn-sm btn-facebook btn-raised"
                                        onclick="$('#filter').modal('show');">
                                    <i class="material-icons">filter_list</i>&nbsp;&nbsp;Filtrar
                                </button>
                                <button class="btn btn-sm reload btn-facebook btn-raised">
                                    <i class="material-icons">replay</i>&nbsp;&nbsp;Recargar
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="material-datatables">
                        <table id="datatables" class="table table-striped table-no-bordered table-hover"
                               cellspacing="0"
                               width="100%" style="width:100%">
                            <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th>Nombre</th>
                                <th>Email</th>
                                <th>Roles</th>
                                <th>Fecha de creacion</th>
                                <th class="text-left">Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                     aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="btn btn-simple close" data-dismiss="modal"
                                        aria-hidden="true">
                                    <i class="material-icons">clear</i>
                                </button>
                                <h4 class="modal-title"></h4>
                            </div>
                            <div class="modal-body">
                                <p class="description-modal"></p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-simple" data-dismiss="modal">Cancelar</button>
                                <button type="button" class="btn btn-danger btn-simple delete" data-dismiss="modal">
                                    Sí
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="filter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                     aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="btn btn-simple close" data-dismiss="modal"
                                        aria-hidden="true">
                                    <i class="material-icons">clear</i>
                                </button>
                                <h4 class="modal-title"></h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <div class="form-group">
                                        <label>ID</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-pencil"></i>
                                            </div>
                                            <input type="text" class="form-control id" placeholder="ID" name="id"
                                                   value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-simple" data-dismiss="modal">Cancelar</button>
                                <button type="button" class="btn btn-danger btn-simple filtrar" data-dismiss="modal">
                                    Filtrar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('javascript')

    <script type="text/javascript">
        $(document).ready(function () {
            $('#datatables').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{route('user.indexAjax')}}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'roles', name: 'roles', orderable:false, searchable:false},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
                columnDefs: [
                    {className: "td-actions text-right", "targets": [5]}
                ],
                "lengthMenu": [
                    [10, 25, 50],
                    [10, 25, 50]
                ],
                responsive: true,
                language: {
                    search: "_INPUT_",
                    searchPlaceholder: "Search records",
                    lengthMenu: "Ver _MENU_ por página",
                    zeroRecords: "No se encontraron coincidencias - :c",
                    info: "Página _PAGE_ de _PAGES_",
                    infoEmpty: "No hay datos",
                    paginate: {
                        first: "Primero",
                        last: "Último",
                        next: "Siguiente",
                        previous: "Anterior"
                    },
                    sProcessing: "<div id=\"contenedor\"><div class=\"loader\" id=\"loader\">Loading...</div></div>"
                }

            });
            var table = $('#datatables').DataTable();

            var theValue;
            $(document).on('click', '.remove', function () {
                $('.description-modal').text("Esta seguro que desea eliminar este registro");
                $('#myModal').modal('show');
                theValue = $(this).data('info');
                console.log(theValue);
            });
            $(document).on('click', '.delete', function (e) {
                $.ajax(
                    {
                        type: "POST",
                        url: '{{route('user.remove')}}',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {
                            user: theValue
                        },
                        success: function () {
                            location.reload();
                        }
                    }
                );
            });
            $(document).on('click', '.reload', function (e) {
                location.reload();
            });
        });
    </script>
    @parent
@stop