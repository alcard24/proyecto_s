@extends('layouts.dashboard')
@section('content')

    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{ Breadcrumbs::render('permission.index') }}

    <div class="header text-center">
        <h3 class="title">Permisos registrados en el sistema.</h3>
        <p class="category">CRUD</p>
    </div>

    {{Form::open()}}
    {{Form::token()}}
    <div class="row">
        <div class="col-lg-6 col-md-12">
            <div class="card">
                <div class="card-content">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group label-floating is-empty">
                                <label for="name" class="control-label">Name</label>
                                <input name="name" type="text"
                                       class="form-control material-input">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group label-floating is-empty">
                                <label for="display_name" class="control-label">Display Name</label>
                                <input name="display_name" type="text"
                                       class="form-control material-input">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group label-floating is-empty">
                                <label for="description" class="control-label">Description</label>
                                <textarea name="description" type="text"
                                          rows="8"
                                          class="form-control material-input"></textarea>
                            </div>
                        </div>

                        <div>
                            <!-- Kavv -->
                            <!-- Change from type="button" to type="submit"-->
                            <button class="btn btn-success" type="submit" id="save" style="margin: 1.5em;">Guardar
                                <div class="ripple-container"/>
                            </button>
                            <button class="btn btn-danger" style="display: none;" id="cancelar"> Cancelar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{Form::close()}}

        <div class="col-lg-6 col-md-12">
            <div class="card">
                <div class="card-content">

                    <div class="toolbar" style="display: inline-block; width: 100%;">
                        <div class="pull-right">
                            <div class="btn-group pull-right" style="margin-right: 10px">
                                <button class="btn btn-sm btn-facebook btn-raised" id="btn_filter">
                                    <i class="material-icons">filter_list</i>&nbsp;&nbsp;Filtrar
                                </button>
                                <button class="btn btn-sm reload btn-facebook btn-raised">
                                    <i class="material-icons">replay</i>&nbsp;&nbsp;Recargar
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="material-datatables">
                        <table
                                id="table-list"
                                class="table table-striped table-no-bordered table-hover"
                                cellspacing="0"
                                width="100%">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Display Name</th>
                                <th>Created At</th>
                                <th >Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="filter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn btn-simple close" data-dismiss="modal"
                            aria-hidden="true">
                        <i class="material-icons">clear</i>
                    </button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="form-group">
                            <label>ID</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-pencil"></i>
                                </div>
                                <input type="text" class="form-control id" placeholder="ID" name="id"
                                       value="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-simple" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-danger btn-simple filtrar" data-dismiss="modal">
                        Filtrar
                    </button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        // Vars
        var isEditing = false;
        var selectedItemId = null;

        $(document).ready(function () {
            $(document).on('click', '.reload', function (event) {
                event.preventDefault();
                location.reload();
            });

            $(document).on('click', '#btn_filter', function (event) {
                event.preventDefault();
                $('#filter').modal('show');
            });

            // Save
            $(document).on('click', '#save', function (event) {
                //Kavv
                //Allow the submit event
                //event.preventDefault();

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: ('{{route('permission.store')}}').concat(isEditing ? '/'.concat(selectedItemId) : ''),
                    type: isEditing ? 'PUT' : 'POST',
                    data: {
                        'name': $('input[name=name]').val(),
                        'display_name': $('input[name=display_name]').val(),
                        'description': $('textarea[name=description]').val()
                    },
                    datatype: 'JSON',
                    success: function () {
                        swal('Guardado', '', 'success');
                        clearComponents();
                    },
                    error: function (msg) {
                        var htmlRaw = '<div class="alert alert-danger"><ul>';
                        var errors = msg.responseJSON.errors;

                        for (issue in errors) {
                            var theProblem = errors[issue];
                            for (error in theProblem) {
                                htmlRaw += '<li>'.concat(theProblem[error].toString()).concat('</li>');
                            }
                        }

                        htmlRaw +='</ul></div>';

                        swal('Error', 'Tenemos los siguientes incovenientes <br/>'.concat(
                            htmlRaw
                        ), 'error');
                    }
                });
            });

            // Table
            $('#table-list').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{route('permission.indexAjax')}}'
                },
                columns: [
                    {data: 'name', name: 'name'},
                    {data: 'display_name', name: 'display_name'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
                columnDefs: [
                    {className: "td-actions text-right", "targets": [3]}
                ],
                "lengthMenu": [
                    [10, 25, 50],
                    [10, 25, 50]
                ],
                responsive: true,
                language: {
                    search: "_INPUT_",
                    searchPlaceholder: "@lang('dashboard.table.search')",
                    lengthMenu: "@lang('dashboard.table.length_menu')",
                    zeroRecords: "@lang('dashboard.table.zero')",
                    info: "@lang('dashboard.table.info')",
                    infoEmpty: "@lang('dashboard.table.empty')",
                    paginate: {
                        first: '@lang('dashboard.table.first')',
                        last: "@lang('dashboard.table.last')",
                        next: "@lang('dashboard.table.next')",
                        previous: "@lang('dashboard.table.previous')"
                    },
                    sProcessing: "<div id=\"contenedor\"><div class=\"loader\" id=\"loader\">Loading...</div></div>"
                }
            });
        });

        function clearComponents() {
            $('input[name=name]').val(null);
            $('input[name=display_name]').val(null);
            $('textarea[name=description]').val(null);
        }

        function filter(event) {
            event.preventDefault();
            $('#filter').modal('show');
        }
    </script>
    @parent
@stop