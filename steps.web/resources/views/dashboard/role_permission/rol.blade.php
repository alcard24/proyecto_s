@extends('layouts.dashboard')
@section('content')

    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{ Breadcrumbs::render('role.index') }}

    <div class="header text-center">
        <h3 class="title">Roles registrados en el sistema.</h3>
        <p class="category">CRUD</p>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-12">
            <div class="card">
                <div class="card-content">

                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-12">
            <div class="card">
                <div class="card-content">

                </div>
            </div>
        </div>
    </div>
@stop