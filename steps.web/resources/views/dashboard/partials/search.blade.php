{{Form::open(array('url'=>$url,'method'=>'GET','autocomplete'=>'off','role'=>'search'))}}
<div class="form-group " >
    <div class="navbar-form">
        <div class="form-group form-primary is-empty">
            <input type="text" class="form-control" name="searchAttribute" placeholder="Buscar..." value="{{$searchAttribute}}">
            <span class="material-input"></span>
        </div>
        <button type="submit" class="btn btn-primary btn-raised btn-fab btn-fab-mini"><i class="material-icons">search</i></button>
    </div>
</div>
{{Form::close()}}