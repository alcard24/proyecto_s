@extends('layouts.dashboard')
@section('content')
    @if(isset($recinto))
        {{ Breadcrumbs::render('enclosure.edit',$enclosure) }}
    @else
        {{ Breadcrumbs::render('enclosure.create') }}
    @endif

    <div class="header text-center">
        <h3 class="title">
            @if(isset($enclosure))
                Modificando recinto {{$enclosure->name}}
            @else
                Agregar un nuevo recinto
            @endif
        </h3>
    </div>

    <div class="col-md-12">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="card">
            <div class="card-content">
                @if(isset($enclosure))
                    {!! Form::model($enclosure, ['url' => ['dashboard/enclosure', $enclosure], 'method' => 'patch'])  !!}
                @else
                    {{ Form::open(['url' => 'dashboard/enclosure']) }}
                @endif


                <input name="latitude" style="display: none;" type="hidden"
                       id="latitude"
                       @if(isset($enclosure))
                       value="{{$enclosure->latitude}}"
                        @endif>
                <input name="longitude"
                       id="longitude"
                       @if(isset($enclosure))
                       value="{{$enclosure->longitude}}"
                       @endif
                       type="hidden"
                       style="display: none;">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group label-floating @if(!isset($enclosure)) is-empty @endif">
                            <label for="name" class="control-label">Nombre del recinto</label>
                            <input name="name" type="text"
                                   @if(isset($enclosure))
                                   value="{{$enclosure->name}}"
                                   @endif
                                   class="form-control material-input">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group label-floating @if(!isset($enclosure)) is-empty @endif">
                            <label for="web" class="control-label">Página web</label>
                            <!-- Kavv -->
                            <!-- Changue from recinto->nombre to recinto->name -->
                            <input name="web" type="url" class="form-control material-input"
                                   @if(isset($enclosure))
                                   value="{{$enclosure->web}}"
                                    @endif>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group label-floating @if(!isset($enclosure)) is-empty @endif">
                            <label for="dir" class="control-label">Dirección</label>
                            <textarea name="address" rows="5"
                                      class="form-control material-input">@if(isset($enclosure)){{$enclosure->address}}@endif</textarea>
                        </div>
                    </div>
                    <div class="col-md-6">

                        @if(isset($ins))
                            @include('dashboard.institution.select',['ins_modal_go'=> '{{$institution->name}}'])
                        @else
                            @include('dashboard.institution.select')
                        @endif
                    </div>

                    <div class="col-md-6 col-sm-12 ">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">search</i>
                            </span>
                            <input type="text" name="mySearchPlace" id="mySearchPlace" class="form-control">
                        </div>
                        <!-- Kavv -->
                        <!-- The original location is not specified-->
                        {!!$data['html']!!}
                    </div>

                </div>
                <button class="btn btn-success" type="submit">Guardar
                    <div class="ripple-container"></div>
                </button>

                {{ Form::close()}}
            </div>
        </div>
    </div>
@stop
@section('javascript')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
    <script>
        $(document).ready(function () {
            $('#instituciones_picker').select2({
                placeholder: "Selecciona la institucion",
                minimumInputLength: 1,
                ajax: {
                    url: '{{url('/ajax/index/institution')}}',
                    dataType: 'json',
                    data: function (params) {
                        return {
                            query: $.trim(params.term)
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                }
            });
        });

        /**
         * Show Notification
         * @param message
         */
        function showNotification(message) {
            $.notify({
                    message: message,
                    type: 'primary',
                    timer: 2000,
                    placement: {
                        from: 'top',
                        align: 'right'
                    }
                }
            );
        }

        /**
         * Show Notification
         * @param message
         * @param type
         */
        function showNotification(message, type) {

            $.notify({
                message: message,
                type: type,
                timer: 2000,
                placement: {
                    from: 'top',
                    align: 'right'
                }
            });
        }
    </script>
    {{--Mapa de seleccion--}}
    {!! $data['js'] !!}
    @parent
@stop