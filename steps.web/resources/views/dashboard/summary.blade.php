@extends('layouts.dashboard')
@section('content')
    {{Breadcrumbs::render('home')}}
    <div class="header text-center">
        <h3 class="title">Resumen de lo que esta pasando</h3>
        <p class="category">Próximos eventos, Graficos informativos</p>
    </div>
    <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="red">
                        <i class="material-icons">pie_chart</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Demánda acádemica</h4>
                    </div>
                    <div id="chartPreferences" class="ct-chart"></div>
                    <div class="card-footer">
                        <i class="fa fa-circle text-info"></i> Ingeniería
                        <i class="fa fa-circle text-warning"></i> Licenciatura
                        <i class="fa fa-circle text-danger"></i> Tecnico
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card card-calendar">
                    <div class="card-content ps-child">
                        <div id="fullCalendar"></div>
                    </div>
                </div>
            </div>
    </div>
@stop