<style type="text/css">
    .modal {
        text-align: center;
    }

    @media screen and (min-width: 768px) {
        .modal:before {
            display: inline-block;
            vertical-align: middle;
            content: " ";
            height: 100%;
        }
    }

    .modal-dialog {
        display: inline-block;
        text-align: left;
        vertical-align: middle;
    }

    .table > tbody > tr.selected > td, .table > tbody > tr.selected > th, .table > tbody > tr > td.selected, .table > tbody > tr > th.selected, .table > tfoot > tr.selected > td, .table > tfoot > tr.selected > th, .table > tfoot > tr > td.selected, .table > tfoot > tr > th.selected, .table > thead > tr.selected > td, .table > thead > tr.selected > th, .table > thead > tr > td.selected, .table > thead > tr > th.selected {
        background-color: #FF9800;
        color: white;
    }
</style>
<div class="form-group label-floating @if(!isset($enclosure)) is-empty @endif" id="instit-label" onclick="$('#selected_ins').modal().show(); ">
    <label for="instit" class="control-label">Selecciona una insitución</label>
    <div class="input-group">
        <input type="hidden" name="institution"
               @if(isset($institution))value="{{$institution->id}}" @endif id="instit_h" style="display: none;" placeholder="">
        <!-- Kavv -->
        <!-- Change from $ins->nombre to $ins->name -->
        <input type="text"
               id="instit"
               @if(isset($institution))value="{{$institution->name}}" @endif
               class="form-control"
               readonly
               autocomplete="false"
               placeholder="">
        <span class="input-group-btn">
                                    <button class="btn btn-default btn-round" type="button">
                                    <i class="material-icons">touch_app</i></button>
                                </span>
    </div>
</div>
<div class="modal fade" id="selected_ins">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn btn-simple close" data-dismiss="modal"
                        aria-hidden="true">
                    <i class="material-icons">clear</i>
                </button>
                <h4 class="modal-title">Selecciona una institución</h4>
            </div>
            <div class="modal-body">
                <div class="material-datatables">
                    <table id="datatables"
                           class="table table-no-bordered table-hover"
                           width="100%"
                           cellspacing="0">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Nombre</th>
                            <th>Fecha de creacion</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer ">
                <button class="btn btn-danger" onclick="$('#selected_ins').modal('hide')">Cancelar</button>
                <button class="btn btn-success" id="ins_modal_go">Seleccionar</button>
            </div>
        </div>
    </div>
</div>
@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#datatables').DataTable({
                processing: true,

                serverSide: true,
                ajax: '{{route('institution.indexAjaxSelect')}}',
                columns: [
                    {data: 'id', name: 'id', orderable: false, searchable: false},
                    {data: 'name', name: 'name'},
                    {data: 'created_at', name: 'created_at'}
                ],
                columnDefs: [
                    {className: "td-actions text-right", "targets": [2]}
                ],
                "lengthMenu": [
                    [5, 10],
                    [5, 10]
                ],
                responsive: true,
                language: {
                    search: "_INPUT_",
                    searchPlaceholder: "Search records",
                    lengthMenu: "Ver _MENU_ por página",
                    zeroRecords: "No se encontraron coincidencias - :c",
                    info: "Página _PAGE_ de _PAGES_",
                    infoEmpty: "No hay datos",
                    paginate: {
                        first: "Primero",
                        last: "Último",
                        next: "Siguiente",
                        previous: "Anterior"
                    },
                    sProcessing: "<div id=\"contenedor\"><div class=\"loader\" id=\"loader\">Loading...</div></div>"
                }

            });
            var table = $('#datatables').DataTable();
            var data = null;
            var nombre = null;
            $('#datatables tbody').on('click', 'tr', function () {
                if ($(this).hasClass('selected')) {
                    $(this).removeClass('selected');
                    data = null;
                    nombre = null;
                }
                else {
                    table.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    data = $(this).find('span').data().id;
                    nombre = $(this).find('span').data().nombre;
                }
            });
            $('#ins_modal_go').on('click', function () {
                if (data == null) {
                    showNotification('Debe seleccionar un institucion primero', 'danger');
                }
                else {
                    $('#selected_ins').modal('hide');
                    var theDOM = $('#instit');
                    var label = $('#instit-label');
                    label.removeClass('is-empty');
                    label.addClass('is-focused');
                    $('#instit_h').val(data);
                    theDOM.val(nombre);
                }
            });
        });
    </script>
    @parent
@stop
