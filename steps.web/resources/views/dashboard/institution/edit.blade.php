@extends('layouts.dashboard')
@section('content')

    <div class="header text-center">
        <h3 class="title">
            @if(isset($institucion))
                Modificando {{$institution->name}} [{{$institution->id}}]
            @else
                Agregar una nueva institución
            @endif
        </h3>
    </div>
    <div class="col-md-12">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="card">

            <div class="card-content">
                @if(isset($institution))
                    {!!  Form::model($institution, ['url' => ['dashboard/institution', $institution->id], 'method' => 'patch']) !!}
                @else
                    {{ Form::open(['url' => 'dashboard/institution']) }}
                @endif

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group label-floating @if(!isset($institution)) is-empty @endif">
                            <label for="name" class="control-label">Nombre de la insitución</label>
                            <input name="name" type="text"
                                   @if(isset($institution))
                                   value="{{$institution->name}}"
                                   @endif
                                   class="form-control material-input">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group label-floating @if(!isset($institution)) is-empty @endif">
                            <label for="web_page" class="control-label">Página web</label>
                            <input name="web_page" type="url"
                                   @if(isset($institution))
                                   value="{{$institution->web_page}}"
                                   @endif
                                   class="form-control material-input">
                        </div>
                    </div>

                    <div class="col-md-12" style="width: 100%">
                            <h3>Misión</h3>
                            <textarea style="width: 100%;"
                                      name="mission">@if(isset($institution)){{$institution->mission}}@endif</textarea>
                    </div>
                    <div class="col-md-12">
                        <h3>Visión</h3>
                            <textarea style="width: 100%;"
                                      name="vision">@if(isset($institution)){{$institution->vision}}@endif</textarea>
                    </div>


                </div>
                <button class="btn btn-success" type="submit">Guardar
                    <div class="ripple-container"></div>
                </button>
                {{ Form::close()}}
            </div>
        </div>
    </div>
@stop
@section('javascript')
    <!-- include summernote css/js -->
    <!-- Import Trumbowyg -->

    {{--<script src="https://rawgit.com/RickStrahl/jquery-resizable/master/dist/jquery-resizable.min.js"></script>--}}
    <script src="{{asset('editor/trumbowyg.js')}}"></script>
    {{--<script src="{{asset('editor/plugin/trumbowyg.resizimg.js')}}"></script>--}}
    <link rel="stylesheet" href="{{asset('editor/ui/css/trumbowyg.css')}}">
    <script>

        $(document).ready(function () {
            $('textarea').trumbowyg({
                plugins: {
                    resizimg : {
                        minSize: 64,
                        step: 16,
                    }
                }
            });
        })
    </script>
    @parent
@stop