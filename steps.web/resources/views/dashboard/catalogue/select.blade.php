<style type="text/css">
    .modal {
        text-align: center;
    }

    @media screen and (min-width: 768px) {
        .modal:before {
            display: inline-block;
            vertical-align: middle;
            content: " ";
            height: 100%;
        }
    }

    .modal-dialog {
        display: inline-block;
        text-align: left;
        vertical-align: middle;
    }

    .table > tbody > tr.selected > td, .table > tbody > tr.selected > th, .table > tbody > tr > td.selected, .table > tbody > tr > th.selected, .table > tfoot > tr.selected > td, .table > tfoot > tr.selected > th, .table > tfoot > tr > td.selected, .table > tfoot > tr > th.selected, .table > thead > tr.selected > td, .table > thead > tr.selected > th, .table > thead > tr > td.selected, .table > thead > tr > th.selected {
        background-color: #FF9800;
        color: white;
    }
</style>

<div class="form-group label-floating is-empty" id="catalogo-label" onclick="$('#selected_ins').modal().show(); ">
    <label for="catalogo_padre_name" class="control-label">Selecciona un catalogo</label>
    <div class="input-group">
        <input type="hidden" name="catalogo_select" id="catalogo_padre_select" style="display: none;" placeholder="" >
        <input type="text" id="catalogo_padre_name" class="form-control" readonly autocomplete="false" placeholder="">
        <span class="input-group-btn">
            <button class="btn btn-default btn-round" type="button">
                <i class="material-icons">touch_app</i>
            </button>
        </span>
    </div>
</div>
<div class="modal fade" id="selected_ins">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn btn-simple close" data-dismiss="modal"
                        aria-hidden="true">
                    <i class="material-icons">clear</i>
                </button>
                <h4 class="modal-title">Selecciona un catalogo</h4>
            </div>
            <div class="modal-body">
                <div class="material-datatables">
                    <table id="data_padre_select"
                           class="table table-no-bordered table-hover"
                           width="100%"
                           cellspacing="0">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Nombre</th>
                            <th>Fecha de creacion</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer ">
                <button class="btn btn-danger" onclick="$('#selected_ins').modal('hide')">Cancelar</button>
                <button class="btn btn-success" id="ins_modal_go">Seleccionar</button>
            </div>
        </div>
    </div>
</div>
@section('javascript')
    <script type="text/javascript">
        var catalogueNotRepeat;
        $(document).ready(function () {
            @if(isset($module) && $module === "quiz")
                route = '{{route('catalogue.indexAjax',"quiz")}}';
            @else
                route = '{{route('catalogue.indexAjax')}}';
            @endif
            $('#data_padre_select').DataTable({
                processing: true,

                serverSide: true,
                ajax: route,
                columns: [
                    {data: 'id', name: 'id', orderable: false, searchable: false},
                    {data: 'name', name: 'name'},
                    {data: 'created_at', name: 'created_at'}
                ],
                columnDefs: [
                    {className: "td-actions text-right", "targets": [2]}
                ],
                "lengthMenu": [
                    [5, 10],
                    [5, 10]
                ],
                responsive: true,
                language: {
                    search: "_INPUT_",
                    searchPlaceholder: "Search records",
                    lengthMenu: "Ver _MENU_ por página",
                    zeroRecords: "No se encontraron coincidencias - :c",
                    info: "Página _PAGE_ de _PAGES_",
                    infoEmpty: "No hay datos",
                    paginate: {
                        first: "Primero",
                        last: "Último",
                        next: "Siguiente",
                        previous: "Anterior"
                    },
                    sProcessing: "<div id=\"contenedor\"><div class=\"loader\" id=\"loader\">Loading...</div></div>"
                }

            });
            let selectTable = $('#data_padre_select').DataTable();
            var data = null;
            var nombre = null;
            $('#data_padre_select tbody').on('click', 'tr', function () {
                if ($(this).hasClass('selected')) {
                    $(this).removeClass('selected');
                    data = null;
                    nombre = null;
                }
                else {
                    selectTable.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    data = $(this).find('span').data().id;
                    nombre = $(this).find('span').data().nombre;
                }
            });
            $('#ins_modal_go').on('click', function () {
                if (data == null) {
                    showNotification('Debe seleccionar un catalogo primero', 'danger');
                }
                else {
                    $('#selected_ins').modal('hide');
                    var theDOM = $('#catalogo_padre_name');
                    var label = $('#catalogo-label');
                    label.removeClass('is-empty');
                    label.addClass('is-focused');
                    $('#catalogo_padre_select').val(data);
                    theDOM.val(nombre);
                }
            });
            $( "#selected_ins" ).on('shown.bs.modal', function(){
                if(catalogueNotRepeat)
                    selectTable.ajax.url('{{route('catalogue.indexAjax')}}'+'?id='+catalogueNotRepeat).load();
                else
                    selectTable.ajax.url('{{route('catalogue.indexAjax')}}').load();
            });
        });
    </script>
    @parent
@stop
