@extends('layouts.dashboard')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{ Breadcrumbs::render('catalogue.index') }}

    <div class="header text-center">
        <h3 class="title">Catalogos registrados en el sistema.</h3>
        <p class="category">Lista de Catalogos</p>
    </div>
    <div class="row">
        <div class="col-lg-5 col-md-12">
            <div class="card">

                <div class="card-content">

                    {{ Form::open(['id' => 'formForSumbit']) }}
                    {{Form::token()}}

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group label-floating is-empty">
                                <label for="nombre" class="control-label">Nombre del catalogo</label>
                                <input name="nombre" type="text"
                                       class="form-control material-input">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group label-floating is-empty">
                                <label for="codigo" class="control-label">Codigo del catalogo</label>
                                <input name="codigo" type="text"
                                       class="form-control material-input">
                            </div>
                        </div>
                        <div class="col-md-12">
                            @include('dashboard.catalogue.select')
                        </div>
                        <div class="col-md-12">
                            <div class="form-group label-floating is-empty">
                                <label for="descripcion" class=" control-label">Descripcion del catalogo</label>
                                <textarea class="form-control"
                                          rows="5"
                                          name="descripcion"></textarea>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12" style="vertical-align: middle; ">
                            <select id="select_icon"
                                    name="icon"
                                    style="width: 100%;"
                            >
                            </select>
                        </div>
                        <div>
                            <button class="btn btn-success" type="button" id="guardar" style="margin: 1.5em;">Guardar
                                <div class="ripple-container"/>
                            </button>
                            <button class="btn btn-danger" style="display: none;" id="cancelar"> Cancelar</button>
                        </div>
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header" data-background-color="orange">
                                    <h4 class="title" style="color: white; text-align: center;">Catalogos asociados</h4>
                                </div>
                                <div class="card-body" style="margin: 1em;">
                                    <div class="material-datatables">
                                        <table
                                                id="data_table_child"
                                                class="table table-striped table-no-bordered table-hover"
                                                cellspacing="0"
                                                width="100%">
                                            <thead>
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Codigo</th>
                                                <th>Opciones</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close()}}
            </div>
        </div>
        <div class=" col-lg-7 col-md-12 ">
            <div class="card">
                <div class="card-content">
                    <div class="toolbar" style="display: inline-block; width: 100%;">
                        <div class="pull-right">
                            <div class="btn-group pull-right" style="margin-right: 10px">
                                <button class="btn btn-sm btn-facebook btn-raised"
                                        onclick="$('#filter').modal('show');">
                                    <i class="material-icons">filter_list</i>&nbsp;&nbsp;Filtrar
                                </button>
                                <button class="btn btn-sm reload btn-facebook btn-raised">
                                    <i class="material-icons">replay</i>&nbsp;&nbsp;Recargar
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="material-datatables">
                        <table id="datatables" class="table table-striped table-no-bordered table-hover"
                               cellspacing="0"
                               width="100%" style="width:100%">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Descripción</th>
                                <th>Codigo</th>
                                <th>Fecha de creacion</th>
                                <th class="text-left">Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                     aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="btn btn-simple close" data-dismiss="modal"
                                        aria-hidden="true">
                                    <i class="material-icons">clear</i>
                                </button>
                                <h4 class="modal-title"></h4>
                            </div>
                            <div class="modal-body">
                                <p class="description-modal"></p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-simple" data-dismiss="modal">Cancelar</button>
                                <button type="button" class="btn btn-danger btn-simple delete" data-dismiss="modal">
                                    Sí
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="filter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                     aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="btn btn-simple close" data-dismiss="modal"
                                        aria-hidden="true">
                                    <i class="material-icons">clear</i>
                                </button>
                                <h4 class="modal-title"></h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <div class="form-group">
                                        <label>ID</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-pencil"></i>
                                            </div>
                                            <input type="text" class="form-control id" placeholder="ID" name="id"
                                                   value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-simple" data-dismiss="modal">Cancelar</button>
                                <button type="button" class="btn btn-danger btn-simple filtrar" data-dismiss="modal">
                                    Filtrar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet"/>

    {{--@stop--}}

    {{--@section('javascript')--}}

    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>

    <script type="text/javascript">
        var selectItemId = null;
        var isEdited = false;
        var dataTable;
        var dataTableChilds;
        $(document).ready(function () {
            selectItemId = null;

            $('#select_icon').select2({
                allowClear: true,
                placeholder: "Seleccione un icono",
                minimumInputLength: 0,
                ajax: {
                    url: '{{route('IconResource.index')}}',
                    method: 'GET',
                    dataType: 'JSON',
                    data: function (params) {
                        return {
                            query: $.trim(params.term)
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data,
                            pagination: {
                                more: false
                            }
                        };
                    },
                    cache: true
                },
                templateResult: function (e) {
                    return $('<p><i class="material-icons" style="vertical-align:middle;">' + e.text + '</i><b style="margin-left: 20px;">' + e.text + '<b/></p>');
                },
                templateSelection: function (e) {

                    if (!e.id) {
                        return 'Selecciona un icono';
                    }
                    else {
                        return $('<p><i class="material-icons" style="vertical-align:middle;">' + e.text + '</i><b style="margin-left: 20px;">' + e.text + '<b/></p>');
                    }
                }
            });
             dataTable = $('#datatables').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{route('catalogue.indexAjax')}}',
                columns: [
                    {data: 'name', name: 'name'},
                    {data: 'description', name: 'description'},
                    {data: 'code', name: 'code', orderable: false, Searchable: false},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
                columnDefs: [
                    {className: "td-actions text-right", "targets": [4]}
                ],
                "lengthMenu": [
                    [10, 25, 50],
                    [10, 25, 50]
                ],
                responsive: true,
                language: {
                    search: "_INPUT_",
                    searchPlaceholder: "Search records",
                    lengthMenu: "Ver _MENU_ por página",
                    zeroRecords: "No se encontraron coincidencias - :c",
                    info: "Página _PAGE_ de _PAGES_",
                    infoEmpty: "No hay datos",
                    paginate: {
                        first: "Primero",
                        last: "Último",
                        next: "Siguiente",
                        previous: "Anterior"
                    },
                    sProcessing: "<div id=\"contenedor\"><div class=\"loader\" id=\"loader\">Loading...</div></div>"
                }

            });
             dataTableChilds = $('#data_table_child').DataTable({
                processing: true,
                serverSide: true,

                ajax: {
                    url: '{{route('catalogue.indexAjaxSelect')}}'
                },
                columns: [
                    {data: 'nombre', name: 'nombre'},
                    {data: 'codigo', name: 'codigo', orderable: false, Searchable: false},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
                columnDefs: [
                    {className: "td-actions text-right", "targets": [2]}
                ],
                "lengthMenu": [
                    [10, 25, 50],
                    [10, 25, 50]
                ],
                responsive: true,
                language: {
                    search: "_INPUT_",
                    searchPlaceholder: "@lang('dashboard.table.search')",
                    lengthMenu: "@lang('dashboard.table.length_menu')",
                    zeroRecords: "@lang('dashboard.table.zero')",
                    info: "@lang('dashboard.table.info')",
                    infoEmpty: '@lang('dashboard.table.empty')',
                    paginate: {
                        first: '@lang('dashboard.table.first')',
                        last: "@lang('dashboard.table.last')",
                        next: "@lang('dashboard.table.next')",
                        previous: "@lang('dashboard.table.previous')"
                    },
                    sProcessing: "<div id=\"contenedor\"><div class=\"loader\" id=\"loader\">Loading...</div></div>"
                }

            });
            var theValue;
            // Primera etapa para eliminar el elmento
            $(document).on('click', '.remove', function () {
                $('.description-modal').text("Esta seguro que desea eliminar este registro");
                $('#myModal').modal('show');
                theValue = $(this).data('info');
            });
            // Confirmacion de eliminar el documento

            $('#cancelar').on('click', function (event) {
                event.preventDefault();
                clear();
            });
            $(document).on('click', '.delete', function () {
                $.ajax(
                    {
                        type: "GET",
                        url: '{{route('catalogue.remove')}}',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {
                            catalogo: theValue
                        },
                        success: function () {
                            location.reload();
                        }
                    }
                );
            });
            $(document).on('click', '.edit', function (event) {
                event.preventDefault();
                id_content = $(this).data('info');
                console.log(id_content);    
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    method: 'GET',
                    url: '{{route('catalogue.find')}}?id=' + id_content,
                    success: function (data) {
                        console.log(data);
                        catalogueNotRepeat = data.id;
                        //KAVV
                        //cambio de data.nombre a data.name
                        $('input[name=nombre]').val(data.name);
                        $('input[name=nombre]').parent().removeClass('is-empty');
                        //KAVV
                        //cambio de data.codigo a data.code
                        $('input[name=codigo]').val(data.code);
                        //Kavv
                        //Propiedad especificada como readonly... 
                        //es posible la agregacion sin especificar el codigo por lo q seria correcto permitir su edicion
                        //o declarar como requerido el campo codigo en la agregacion
                        $('input[name=codigo]').prop('readonly', true);
                        $('input[name=codigo]').parent().removeClass('is-empty');
                        //KAVV
                        //cambio de data.descripcion a data.description
                        $('textarea[name=descripcion]').val(data.description);
                        $('textarea[name=descripcion]').parent().removeClass('is-empty');
                        if (data.get_icon != null) {
                            icon = $('<p><i class="material-icons" style="vertical-align:middle;">' + data.get_icon.name + '</i><b style="margin-left: 20px;">' + data.get_icon.name + '</b></p>');

                            var newOption = new Option(data.name, data.id, false, false);
                            $('select[name=icon]')
                                .empty()
                                .append('<option selected value="' + data.get_icon.id + '">' + data.get_icon.name + '</option>');
                            $('select[name=icon]').trigger('change');
                        }

                        
                        //KAVV
                        //Esto esta duplicado, q procede ?
                        $('input[name=nombre]').val(data.name);
                        $('input[name=nombre]').parent().removeClass('is-empty');

                        selectItemId = data.id;
                        isEdited = true;

                        if (data.get_master != null) {

                            $('input[name=catalogo_select]').val(data.get_master.id);
                            //KAVV
                            //change get_master.nombre to get_master.name
                            $('#catalogo_padre_name').val(data.get_master.name);
                            $('#catalogo_padre_name').parent().parent().removeClass('is-empty');
                        }

                        $('#cancelar').css({display: "initial"});
                        // dataTableChilds.ajax;
                        //Kavv
                        //Cambio de la ruta catalogue.indexAjaxSelect por 
                        dataTableChilds.ajax.url('{{route('catalogue.indexAjaxSelect')}}' + '?idSeleccionado=' + selectItemId).load();
                        //dataTableChilds.ajax.reload();
                    },
                    error: function () {
                        swal('Error', '', 'error');
                    }
                });
            });
            $(document).on('click', '.reload', function (event) {
                event.preventDefault();
                location.reload();
            });
            $('#guardar').on('click', function (event) {
                event.preventDefault();
                var theData = {
                    'nombre': $('input[name=nombre]').val(),
                    'codigo': $('input[name=codigo]').val(),
                    'descripcion': $('textarea[name=descripcion]').val(),
                    'icon': $('select[name=icon]').val(),
                    'catalogo_padre_select': $('input[name=catalogo_select]').val()
                };
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: ('{{route('catalogue.store')}}').concat(isEdited === false ? '' : ('/' + selectItemId )),
                    type: isEdited === false ? 'POST' : 'PUT',
                    data: theData,
                    success: function (data) {
                        swal('Guardado', '', 'success');
                        clear();
                    },
                    error: function (data) {
                        swal('Error', 'Tenemos los siguientes inconvenientes <br/>' + data.responseJSON.htmlRaw, 'error');
                    }

                }); // Ignore normal action;
            });
        });

        function clear() {
            isEdited = false;
            selectItemId = null;

            $('input[name=nombre]').val(null);
            $('input[name=codigo]').val(null);
            $('input[name=catalogo_select]').val(null);
            $('#catalogo_padre_name').val(null);
            $('#catalogo_padre_name').parent().parent().addClass('is-empty');

            $('input[name=nombre]').parent().addClass('is-empty');
            $('input[name=codigo]').val(null);
            $('input[name=codigo]').prop('readonly', false);
            $('input[name=codigo]').parent().addClass('is-empty');
            $('input[name=catalogo_select]').val(null);
            $('input[name=catalogo_select]').parent().addClass('is-empty');

            $('textarea[name=descripcion]').val('');
            $('textarea[name=descripcion]').parent().addClass('is-empty');
            $('#cancelar').css({display: "none"});

            $('select[name=icon]')
                .empty();
            $('select[name=icon]').trigger('change');
            dataTable.ajax.reload();
            dataTableChilds.ajax.reload();
        }
    </script>
    @parent
@stop