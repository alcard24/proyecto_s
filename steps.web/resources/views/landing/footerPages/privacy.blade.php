<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Privacidad</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('privacidad.name', 'privacidad') }}</title>

    <!-- Styles -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="{{asset('css/material-kit.css')}}" rel="stylesheet" />
    <link type="image/jpg" href="{{asset('img/imagen_contacto.jpg')}}" />
</head>

<body>
    <div>
        @include('landing.navbar')
    </div>

    <container-fluid>

        <div class="header-support" class="responsive" class="col-xs-12 col-sm-12 col-md-12">
            <div class="header">
                <img src="{{asset('img/privacidad.jpg')}}" class="img-responsive" style="width:100%" color=white alt="Privacidad" height="283" width="213">
            </div>

            <div>
                <h2 align="center">
                    NUESTRO COMPROMISO

                </h2>
                <blockquote class="blockquote">
                    <p class="mb-0" align=justify>En INFO-EDU, nuestra prioridad principal es proteger tu información personal. Al reconocer plenamente el hecho de que tu información personal te pertenece, hacemos todo lo posible para almacenar con seguridad y procesar con cuidado
                        la información que compartes con nosotros. Ponemos nuestro valor más alto en tu confianza. Por ende, recopilamos una cantidad mínima de información solo con tu permiso y la usamos únicamente a los efectos previstos. No les proporcionamos
                        la información a terceros sin tu conocimiento. Todos en INFO-EDU, hacemos nuestro mayor esfuerzo para garantizar la protección de tus datos, incluidos la seguridad de los datos técnicos y procedimientos de gestión interna, como
                        así también medidas para la protección física de los datos. Nos esforzamos en mejorar sus vidas al ofrecer experiencias digitales inspiradoras y fascinantes. A fin de lograrlo, tu confianza es de suma importancia y, por consiguiente,
                        haremos todo lo posible para proteger tu información personal. Gracias por tu interés y apoyo continuos.
                    </p>
                </blockquote>

                <h2 align="center">
                    Política de privacidad
                </h2>
                <blockquote class="blockquote">
                    <p class="mb-0" align=justify>
                        Politica de Privacidad de INFO-EDU. para Internet Siempre que sea posible, procura manejar la información que recibe de usted, a través de Internet, con un cuidado razonable bajo las circunstancias. INFO-EDU usa la información provista por usted (o por
                        cualquier otro usuario) para medir el uso de su sitio y mejorar sus contenidos. La información personal que nosotros recolectamos es usada solamente por nosotros para responder a sus consultas, procesar una orden o permitirle acceder
                        a una información específica. En oportunidades utilizamos "cookies" para proveernos de cierta información. Una "cookie" es un pequeño elemento de datos que un sitio web envía a su navegador, quien lo almacena en su disco rígido
                        de manera que nosotros podamos reconocerlo cuando usted vuelve a visitar nuestro sitio web. Usted puede configurar su navegador para ser notificado cuando usted recibe una "cookie". El sitio web de INFO-EDU contiene "links" (enlaces)
                        a otros sitios web. Nosotros tratamos de "linkearnos" a sitios que comparten nuestros altos estándares y respeto por la privacidad, pero no somos responsables por el contenido o las políticas de privacidad empleadas por otros sitios.
                    </p>
                </blockquote>
            </div>



        </div>



    </container-fluid>
    @include('landing.modal') @include('landing.footer')


    <!-- Librería jQuery requerida por los plugins de JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Todos los plugins JavaScript de Bootstrap (también puedes
         incluir archivos JavaScript individuales de los únicos
         plugins que utilices) -->
    <script src="js/bootstrap.min.js"></script>
</body>
</html