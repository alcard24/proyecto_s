<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('acerca.name', 'acerca de') }}</title>

    <!-- Styles -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="{{asset('css/material-kit.css')}}" rel="stylesheet" />
    <link type="image/jpg" href="{{asset('img/imagen_contacto.jpg')}}" />
</head>

<body>
    <div>
        @include('landing.navbar')
    </div>

    <div class="header">
        <img src="{{asset('img/imagen_contacto.jpg')}}" class="img-responsive" style="width:100%" color=white alt="Acerca de nostros">
    </div>

    <div class="row" class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group row" align="center">

            <div class="col-md-4">
                <div class="card text-blue bg-primary  mb-3" style="max-width: 32rem;">
                    <div class="card-body">
                        <h5 class="card-title" aling="center">
                            INFO-EDU
                        </h5>
                        <hr>
                        <p class="card-text">Tenemos como proposito ofrecer un motor de busqueda que facilite encontrar carreras especificas asi como su pensum e universidades que las imparten</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card text-blue bg-primary  mb-3" style="max-width: 32rem;">
                    <div class="card-body">
                        <h5 class="card-title" aling="center">
                            Alcances de INFO-EDU
                        </h5>
                        <hr>
                        <p class="card-text">facilita la búsqueda de la oferta educativa Técnica y universitaria así mismo el emparejamiento vocacional ayudando a las personas a encontrar su carrera ideal.</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card text-blue bg-primary  mb-3" style="max-width: 32rem;">
                    <div class="card-body">
                        <h5 class="card-title" aling="center">
                            Vision de INFO-EDU
                        </h5>
                        <hr>
                        <p class="card-text">Nuestra vision es la de diseñar una plataforma informática para la Orientación Vocacional, La gestión de la Oferta y la Demanda Académica Nicaragüense.</p>
                    </div>
                </div>
            </div>

        </div>



    </div>
    </div><br> @include('landing.footer')
<script src="js/bootstrap.min.js"></script>

</html>