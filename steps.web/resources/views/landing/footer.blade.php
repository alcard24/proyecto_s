<footer class="footer footer-black">
    <div class="container">
        <a class="footer-brand" href="#">Bowtie &reg; </a>
        <ul class="pull-center">
            <li>
                <a href="{{ url('/about') }}">
                    @lang('shared.about')
                </a>
            </li>
            <li>
                <a href="#">
                    @lang('shared.terms')
                </a>
            </li>
            <li>
                <a href="{{ url('/privacy') }}">
                    @lang('shared.privacy')
                </a>
            </li>
            <li>
                <a id="modalTag" href="#exampleModal" data-toggle="modal" data-target="#exampleModal"
                   data-whatever="@mdo">
                    @lang('shared.contact')
                </a> @include('landing.modal')
            </li>
        </ul>

        <ul class=" social-buttons pull-right ">

            <li>
                <a href="https://twitter.com/frederamils " target="_blank " class="btn btn-just-icon btn-simple ">
                    <i class="fa fa-twitter "></i>
                </a>
            </li>
            <li>
                <a href="https://www.facebook.com/ " target="_blank " class="btn btn-just-icon btn-simple ">
                    <i class="fa fa-facebook-square "></i>
                </a>
            </li>
            <li>
                <a href="https://www.instagram.com/ " target="_blank " class="btn btn-just-icon btn-simple ">
                    <i class="fa fa-instagram "></i>
                </a>
            </li>
        </ul>
    </div>

</footer>

<script>
    $('#modalTag').on('click', function () {
        $('#exampleModal').modal('show');

    });
</script>