@extends('landing.template')
@section('header')

    <div class="page-header header-filter" style="background-image:url('{{asset('img/wallper2.jpg')}}');">
        <div class="container" style>
            <div class="row">
                <div class="col-md-12" id="presentation">
                    <h1 class="title">@lang('shared.slogan_title')</h1>
                    <h4>@lang('shared.slogan_subtitle')</h4>
                    <br/>
                    <div class="card card-raised card-form-horizontal">
                        <div class="card-content">
                            <form method="" action="">
                                <div class="row">
                                    <div class="col-sm-5">

                                        <div class="input-group">
                                            <span class="input-group-addon">
	        										<i class="material-icons">search</i>
                                            </span>
                                            <div class="form-group is-empty">
                                                <input value="" placeholder="@lang('shared.what')" class="form-control">
                                                <span class="material-input">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">

                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">location_on</i>
                                            </span>
                                            <div class="form-group is-empty"><input value="" placeholder="@lang('shared.where')"
                                                                                    class="form-control"><span
                                                        class="material-input"></span></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="button" class="btn btn-primary btn-block">@lang('shared.search')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('body')

    <div class="main">
        <div class="blog-1">
            <div class="container">
                <div class="row">

                    <div class="col-md-10 col-md-offset-1">
                        <div class="col-md-10 col-md-offset-1">
                            <h2 class="title">Test vocacinales</h2>

                            <br>
                            <div class="card card-plain card-blog">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="card-image">
                                            <img class="img img-raised" src="{{asset('img/kuder.jpg')}}">
                                            <div class="colored-shadow"
                                                 style="background-image: url('{{asset('img/kuder.jpg')}}'); opacity: 1;"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <h6 class="category text-info">Kuder</h6>
                                        <h3 class="card-title">
                                            <a href="#">Escala de preferencias vocacionales de kuder</a>
                                        </h3>
                                        <p class="card-description">
                                            Es una prueba objetiva que permite descubrir las áreas generales donde se
                                            sitúan los intereses y las preferencias del individuo, respecto a la
                                            vocación.
                                            <a href="#pablo"> Realizar el test </a>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="card card-plain card-blog">
                                <div class="row">
                                    <div class="col-md-7">
                                        <h6 class="category text-danger">
                                            Raven
                                        </h6>
                                        <h3 class="card-title">
                                            <a href="#pablo"> Consiste en encontrar la pieza faltante en una serie de
                                                figuras que se irán mostrando.</a>
                                        </h3>
                                        <p class="card-description">
                                            Se trata de un test no verbal, donde el sujeto describe piezas faltantes de
                                            una serie de láminas pre-impresas. Se pretende que el sujeto utilice
                                            habilidades perceptuales, de observación y razonamiento analógico para
                                            deducir el faltante en la matriz. Se le pide al paciente que analice la
                                            serie que se le presenta y que siguiendo la secuencia horizontal y vertical.<a
                                                    href="#pablo"> Realizar el test </a>
                                        </p>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="card-image">
                                            <img class="img img-raised" src="{{asset('img/raven.jpg')}}">
                                            <div class="colored-shadow"
                                                 style="background-image: url('{{asset('img/raven.jpg')}}'); opacity: 1;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop