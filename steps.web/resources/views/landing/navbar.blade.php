<nav class="navbar navbar-default navbar-transparent navbar-fixed-top navbar-color-on-scroll" color-on-scroll="100">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
        		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example">
            		<span class="sr-only">Toggle navigation</span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
        		</button>
        </div>
        <div class="collapse navbar-collapse" >
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="{{url('/')}}">@lang('landing.home')</a></li>
                <li><a href="{{url('/careers')}}">@lang('landing.careers')</a></li>
                <li><a href="#">@lang('landing.employment')</a></li>
                <li><a href="#">@lang('landing.test')</a></li>
                <li>                    
                    @if (Auth::check())
                        <a href="{{ url('/dashboard') }}" class="btn btn-danger btn-raised btn-round">@lang('landing.control_panel')</a>
                    @else
                        <a href="{{ url('/login') }}" class="btn btn-danger btn-raised btn-round">@lang('landing.login')</a>
                    @endif
                </li>
            </ul>
        </div>
    </nav>