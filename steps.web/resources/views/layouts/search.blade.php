@extends('landing.template')
@section('header')
    <div class="page-header header-filter header-small" data-parallax="true"
         style="background-image: url({{asset('img/lock.jpeg')}});">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="brand">
                        <h1 class="title">Oferta academica</h1>
                        <h4>Accede a una lista de carreras ofrecidas en <b>Nicaragua</b> de manera gratuita</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('body')
    <div class="main ">

        <div class="section">
            <div class="container">
                <h2 class="section-title">Oferta</h2>
                <div class="row">
                    <div class="col-md-3">
                        <div class="card card-refine card-plain">
                            <div class="card-content">
                                <h4 class="card-title">
                                    Filtros
                                    <button class="btn btn-default btn-fab btn-fab-mini btn-simple pull-right"
                                            rel="tooltip"
                                            title="Reset Filter">
                                        <i class="material-icons">cached</i>
                                    </button>
                                </h4>
                                <div class="panel panel-default panel-rose">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                        <a class="collapsed" role="button" data-toggle="collapse"
                                           data-parent="#accordion"
                                           href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                            <h4 class="panel-title">Titulo</h4>
                                            <i class="material-icons">keyboard_arrow_down</i>
                                        </a>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel"
                                         aria-labelledby="headingThree">
                                        <div class="panel-body">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="" data-toggle="checkbox" checked="">
                                                    Todos
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="" data-toggle="checkbox">
                                                    Tecnico medio
                                                </label>
                                            </div>

                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="" data-toggle="checkbox">
                                                    Profesional
                                                </label>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div><!-- end card -->
                    </div>

                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-4" >
                                <div class="card card-product card-plain no-shadow" data-colored-shadow="true">
                                    <div class="card-image">
                                        <a href="#">
                                            <img src="{{asset('img/carrera-universitaria.png')}}"
                                                 alt=..."/>
                                        </a>
                                    </div>
                                    <div class="card-content">
                                        <a href="#">
                                            <h4 class="card-title">
                                                ESCUELA NACIONAL DE MÚSICA “LUIS ABRAHAM DELGADILLO”
                                            </h4>
                                        </a>
                                    </div>
                                </div> <!-- end card -->
                            </div>
                            <div class="col-md-4">
                                <div class="card card-product card-plain no-shadow" data-colored-shadow="true">
                                    <div class="card-image">
                                        <a href="#">
                                            <img src="{{asset('img/carrera-universitaria.png')}}"
                                                 alt="..."/>
                                        </a>
                                    </div>
                                    <div class="card-content">
                                        <a href="#">
                                            <h4 class="card-title">
                                                ESCUELA NAC. DE ARTES PLÁSTICAS “RODRIGO PEÑALBA”
                                            </h4>
                                        </a>

                                    </div>
                                </div> <!-- end card -->
                            </div>
                            <div class="col-md-4">
                                <div class="card card-product card-plain no-shadow" data-colored-shadow="true">
                                    <div class="card-image">
                                        <a href="#">
                                            <img src="{{asset('img/carrera-universitaria.png')}}"
                                                 alt="..."/>
                                        </a>
                                    </div>
                                    <div class="card-content">
                                        <a href="#">
                                            <h4 class="card-title">ESCUELA NACIONAL DE DANZA “ADAN CASTILLO”</h4>
                                        </a>
                                    </div>
                                </div> <!-- end card -->
                            </div>

                            <div class="col-md-4">
                                <div class="card card-product card-plain no-shadow" data-colored-shadow="true">
                                    <div class="card-image">
                                        <a href="#">
                                            <img src="{{asset('img/carrera-universitaria.png')}}"
                                                 alt="..."/>
                                        </a>
                                    </div>
                                    <div class="card-content">
                                        <a href="#">
                                            <h4 class="card-title">
                                                ESCUELA NACIONAL DE BALLET
                                            </h4>
                                        </a>

                                    </div>
                                </div> <!-- end card -->
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop