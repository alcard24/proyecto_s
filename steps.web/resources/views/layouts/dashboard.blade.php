<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('img/apple-icon.png')}}"/>
    <link rel="icon" type="image/png" href="{{asset('img/favicon.png')}}"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>Info-edu</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width"/>
    <!-- Bootstrap core CSS     -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet"/>
    <!--  Material Dashboard CSS    -->
    <link href="{{asset('css/material-dashboard.css')}}" rel="stylesheet">
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('css/material-icons.css')}}"/>
    <link href="{{asset('css/demo.css')}}" rel="stylesheet"/>
    <!--   Core JS Files   -->
    <script src="{{asset('js/jquery-3.1.1.min.js')}}" type="text/javascript"></script>
    <link rel="stylesheet" href="{{asset('/css/loader.css')}}">
</head>

<body>
<div class="wrapper">
    <div class="sidebar" data-active-color="orange" data-background-color="black" data-image="{{asset('img/wallper2.jpg')}}">

        <div class="sidebar-wrapper ">
            <div class="logo">
                <a href="{{URL::to('/')}}" class="simple-text">
                    ST3PS
                </a>
            </div>
            <div class="logo logo-mini">
                <a href="{{URL::to('/')}}" class="simple-text">
                    ST3PS
                </a>
            </div>
            <div class="user">
                <div class="photo" style="background-color: white;">
                    <img src="{{Auth::user()->avatar}}"/>
                </div>
                <div class="info">
                    <a>{{Auth::user()->name}}</a>
                </div>
            </div>
            <ul class="nav">

                <li class="{{active(URL::route('home'))}}">
                    <a href="{{URL::route('home')}}">
                        <i class="material-icons">dashboard</i>
                        <p>Resumen</p>
                    </a>
                </li>
                <li>
                    <a data-toggle="collapse" href="#formsExamples">
                        <i class="material-icons">content_paste</i>
                        <p>Listar ofertas
                            <b class="caret"></b>
                        </p>
                    </a>
                    <div class="collapse" id="formsExamples">
                        <ul class="nav">
                            <li>
                                <a href="">Nueva oferta acádemica</a>
                            </li>
                            <li>
                                <a href="#">Modificar ofertas</a>
                            </li>
                        </ul>
                    </div>
                </li>


                <li>
                    <a name="recintos" data-toggle="collapse" href="#mapsExamples">

                        <i class="material-icons">place</i>
                        <p>Recintos
                            <b class="caret"></b>
                        </p>
                    </a>
                    <div class="collapse {{active([URL::action('EnclosureController@index'),URL::action('EnclosureController@create')],'in')}}"
                         id="mapsExamples">
                        <ul class="nav">
                            <li class="{{active([URL::action('EnclosureController@index')])}}">
                                <a href="{{URL::action('EnclosureController@index')}}">Listar recintos</a>
                            </li>
                            <li class="{{active([URL::action('EnclosureController@create')])}}">
                                <a href="{{URL::action('EnclosureController@create')}}">Agregar recintos</a>
                            </li>
                        </ul>
                    </div>
                </li>

                @if(Entrust::hasRole('developer'))
                    <li class="{{active(URL::action('CatalogueController@index'))}}">
                        <a href="{{URL::action('CatalogueController@index')}}">
                            <i class="material-icons">inbox</i>
                            <p>Catalogos
                            </p>
                        </a>
                    </li>
                    <li>
                        <a data-toggle="collapse" href="#insti">
                            <i class="material-icons">business</i>
                            <p>Instituciones
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse {{active([URL::action('InstitutionController@index'),URL::action('InstitutionController@create')],'in')}}"
                             id="insti">
                            <ul class="nav">
                                <li class="{{active([URL::action('InstitutionController@index')])}}">
                                    <a href="{{URL::action('InstitutionController@index')}}">Listar</a>
                                </li>
                                <li class="{{active([URL::action('InstitutionController@create')])}}">
                                    <a href="{{URL::action('InstitutionController@create')}}">Agregar institución</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a data-toggle="collapse" href="#rol">
                            <i class="material-icons">verified_user</i>
                            <p>Roles & Permisos
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse {{active([URL::action('RoleController@index'),URL::action('PermissionController@index')],'in')}}" id="rol">
                            <ul class="nav">
                                <li class="{{active([URL::action('RoleController@index')])}}">
                                    <a href="{{URL::action('RoleController@index')}}">Roles</a>
                                </li>
                                <li class="{{active([URL::action('PermissionController@index')])}}">
                                    <a href="{{URL::action('PermissionController@index')}}">Permisos</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                @endif

                @if(Entrust::hasRole('developer'))
                    <li>
                        <a data-toggle="collapse" href="#user">
                            <i class="material-icons">person</i>
                            <p>@lang("dashboard.users")
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse" id="user">
                            <ul class="nav">
                                <li>
                                    <a href="{{URL::action('UserController@index')}}">@lang("dashboard.users.show")</a>
                                </li>
                                <li>
                                    <a href="{{URL::action('UserController@create')}}">@lang("dashboard.users.add")</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    
                    <li >

                        <a data-toggle="collapse" href="#career">
                            <i class="material-icons">account_balance</i>
                                @lang("dashboard.careers")
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse {{active([URL::action('CareerController@index'),URL::action('CareerController@create')],'in')}}" id="career">
                            <ul class="nav">
                                <li class="{{active([URL::action('CareerController@index')])}}">
                                    <a href="{{URL::action('CareerController@index')}}">Show Career</a>
                                </li>
                                <li class="{{active([URL::action('CareerController@create')])}}">
                                    <a href="{{URL::action('CareerController@create')}}">Add Career</a>
                                </li>
                            </ul>
                        </div>
                    </li>

                @endif
            </ul>
        </div>
    </div>


    <div class="main-panel">
        <nav class="navbar navbar  navbar-absolute" >

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#" data-image="{{asset('img/wallper2.jpg')}}"> Panel de control </a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="material-icons">notifications</i>
                                <span class="notification">5</span>
                                <p class="hidden-lg hidden-md">
                                    @lang('dashboard.notifications')
                                    <b class="caret"></b>
                                </p>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="#">@lang('dashboard.notifications')</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="material-icons">person</i>
                                <p class="hidden-lg hidden-md">Profile</p>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        @lang("dashboard.logout")
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>

                        <li class="separator hidden-lg hidden-md"></li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="content">
            <div class="container-fluid">
                @yield('content')
            </div>
        </div>
    </div>
</div>
</body>
<script src="{{asset('js/jquery-ui.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/material.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/perfect-scrollbar.jquery.min.js')}}" type="text/javascript"></script>
<!-- Forms Validations Plugin -->
<script src="{{asset('js/jquery.validate.min.js')}}"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="{{asset('js/moment.min.js')}}"></script>
<!--  Charts Plugin -->
<script src="{{asset('js/chartist.min.js')}}"></script>
<!--  Plugin for the Wizard -->
<script src="{{asset('js/jquery.bootstrap-wizard.js')}}"></script>
<!--  Notifications Plugin    -->
<script src="{{asset('js/bootstrap-notify.js')}}"></script>
<!-- DateTimePicker Plugin -->
<script src="{{asset('js/bootstrap-datetimepicker.js')}}"></script>
<!-- Vector Map plugin -->
<script src="{{asset('js/jquery-jvectormap.js')}}"></script>
<!-- Sliders Plugin -->
<script src="{{asset('js/nouislider.min.js')}}"></script>
<!-- Select Plugin -->
{{--<script src="{{asset('js/jquery.select-bootstrap.js')}}"></script>--}}
<!--  DataTables.net Plugin    -->
<script src="{{asset('js/jquery.datatables.js')}}"></script>
<!-- Sweet Alert 2 plugin -->
<script src="{{asset('js/sweetalert2.js')}}"></script>
<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="{{asset('js/jasny-bootstrap.min.js')}}"></script>
<!--  Full Calendar Plugin    -->
<script src="{{asset('js/fullcalendar.min.js')}}"></script>
<!-- TagsInput Plugin -->
<script src="{{asset('js/jquery.tagsinput.js')}}"></script>
<!-- Material Dashboard javascript methods -->
<script src="{{asset('js/material-dashboard.js')}}"></script>
@yield('javascript')

</html>
