<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 6/28/18 10:15 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */
return [

    /*
    |--------------------------------------------------------------------------
    | Dashboard Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during Dashboard for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    "notifications" => "Notifications",
    "logout" => "Login out",
    "careers" => "Careers",
    "users" => "Users",
    "users.show" => "Show Users",
    "users.add" => "Add Users",

    "table.length_menu" => "See _MENU_ per page",
    "table.search" => "Search Records",
    "table.info" => "page _PAGE_ of _PAGES_",
    "table.empty" => "No data",
    "table.first" => "First",
    "table.last" => "Last",
    "table.next" => "Next",
    "table.previous" => "Previous",
    "table.zero" => "No Records Matched 😢"
];