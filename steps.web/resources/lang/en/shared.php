<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 6/28/18 10:41 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */
return [
    'where' => 'Where',
    'what' => 'What',
    'search' => 'Search',
    'slogan_title' => 'Allow us to accompany you on your way',
    'slogan_subtitle' => 'Steps is a platform for Vocational orientation and employment exchange',
    'about' => 'About Us',
    'terms' => 'Terms & Conditions',
    'privacy'  => 'Privacy',
    'contact' => 'Contact Us'
];