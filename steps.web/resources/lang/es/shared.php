<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 6/28/18 10:43 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */
return [
    'where' => 'Where',
    'what' => 'What'
];