<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 6/28/18 10:16 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Landing Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during Landing for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

];