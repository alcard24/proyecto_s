import Vue from 'vue'
import Router from 'vue-router'

import User from '../views/users/User'

Vue.use(Router);


export default new Router({
    mode: 'history', // https://router.vuejs.org/api/#mode
    linkActiveClass:
        'open active',
    scrollBehavior:
        () => ({y: 0}),
    routes:
        [
            {
                path: '/',
                redirect: '/vue',
                name: 'Home',
                component: User
            }
        ]
});
