<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Feature\Auth;
use Tests\TestCase;

class UserLoginTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    function loads_login()
    {
        $this->get('/login')
        	->assertStatus(200)
        	->assertSee('Inicia Sesión');
    }

    /** @test */
    function loads_register()
    {
        $this->get('/register')
        	->assertStatus(200)
        	->assertSee('Registrate');
    }

    /** @test */
    function do_login()
    {
        $user = factory(\App\Steps\User\User::class)->create();
        \Auth::loginUsingId($user->id);
        $respuesta = $this->get('/dashboard');
        $respuesta->assertStatus(200);
    }

    /** @test */
    function dont_load_dashboard_without_login()
    {
        $this->get('/dashboard')
            ->assertStatus(302);
    }

        /** @test */
    function loads_dashboard_with_login()
    {
         $user = factory(\App\Steps\User\User::class)->create();
        \Auth::loginUsingId($user->id);
        $this->get('/dashboard')
            ->assertStatus(200);
    }
}
