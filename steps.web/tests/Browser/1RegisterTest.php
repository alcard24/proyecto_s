<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class RegisterTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function testIsRegisterPage()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/register')
                    ->assertSee('Registrate');
        });
    }
     public function testRegister()
    {
        $this->browse(function($browser)
     {
     $browser->visit('/register')
             ->type('name','admin')
             ->type('email', 'admin@gmail.com')
             ->type('password', 'a_developer')
             ->type('password_confirmation', 'a_developer')
             ->press('register')
             ->assertPathIs('/dashboard');
     });
    }
}
