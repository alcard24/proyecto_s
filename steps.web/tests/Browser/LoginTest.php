<?php

namespace Tests\Browser;

use App\Steps\User\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\DuskTestCase;

class LoginTest extends DuskTestCase
{
    use DatabaseMigrations;
    protected $user;
 
    public function setUp()
    {
    parent::setUp();
    $this->user = factory(User::class)->create([
    'email' => 'fredd@gmaill.com',
    'password' => bcrypt('a_developer'),
    'name' => 'admin'
    ]);
    }
 
    public function testIsLoginPage()
    {
        $this->browse(function ($browser) {
         $browser->visit('/login')
             ->assertTitle('Steps')
             ->assertSee('Forgot Your Password?')
             ->clickLink("Forgot Your Password?")
             ->assertPathIs('/password/reset')
             ->assertSee('Reset Password');
        });
    }
 
 
    public function testLogin()
    {
        $this->browse(function($browser)
     {
     $browser->visit('/login')
             ->type('email', $this->user->email)
             ->type('password', 'a_developer')
             ->press('login')
             ->assertPathIs('/dashboard');
     });
    }
}
