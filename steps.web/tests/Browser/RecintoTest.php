<?php

namespace Tests\Browser;

use App\Steps\User\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class RecintoTest extends DuskTestCase
{
    use DatabaseMigrations;
    protected $user;

    public function setUp()
    {
    parent::setUp();
    $this->user = factory(User::class)->create([
    'email' => 'fredd@gmaill.com',
    'password' => bcrypt('a_developer'),
    'name' => 'admin'
    ]);
    }

    public function testAddRecinto()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->user->id)
            ->visit('/dashboard')
            ->clickLink('recintos')
            ->clickLink('Listar recintos')
            ->assertPathIs('/dashboard/recinto')
            ->assertSee('Nombre')
            ->press('nuevoRecinto')
            ->assertPathIs('/dashboard/recinto/create');
        });
    }
}
