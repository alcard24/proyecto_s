<?php
/**
 * @fopyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 6/28/18 9:00 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

//------------------------<Landing routes>-----------------------------------------------------------------------
Route::get('/', function () {
    return view('/landing/index');
});
Route::get('index', function () {
    return view('/landing/index');
});

//------------------------<Landing routes>-----------------------------------------------------------------------


//------------------------<Authentication routes>-----------------------------------------------------------------------
Auth::routes();
//------------------------<Authentication routes>-----------------------------------------------------------------------

Route::get('/dashboard', 'HomeController@index')->name('home');

Route::group(['middleware' => ['role:developer']], function () {

    //------------------------<Rol routes>-----------------------------------------------------------------------------
    Route::resource('/dashboard/role', 'RoleController');
    Route::get('/dashboard/role/retrieveAll', 'RoleController@retrieveAllRoles')->name('role.retrieveAll');
    //------------------------<Rol routes>-----------------------------------------------------------------------------

    //------------------------<Permission routes>-----------------------------------------------------------------------
    Route::resource('/dashboard/permission', 'PermissionController');
    Route::get('/ajax/index/permission', 'PermissionController@indexAjax')->name('permission.indexAjax');
    //------------------------<Permission routes>-----------------------------------------------------------------------

    //------------------------<Enclosure routes>------------------------------------------------------------------------
    Route::resource('/dashboard/enclosure', 'EnclosureController');
    Route::get('/ajax/delete/enclosure', 'EnclosureController@ajaxDelete')->name('enclosure.remove');
    Route::get('/ajax/index/enclosure', 'EnclosureController@ajaxIndex')->name('enclosure.indexAjax');
    //------------------------<Enclosure routes>------------------------------------------------------------------------

    //------------------------<User routes>----------------------------------------------------------------------------
    Route::resource('/dashboard/user', 'UserController');
    Route::post('/ajax/delete/user', 'UserController@ajaxDelete')->name('user.remove');
    Route::get('/ajax/index/user', 'UserController@ajaxIndex')->name('user.indexAjax');
    Route::post('/ajax/delete/user', 'UserController@ajaxDelete')->name('user.remove');
    Route::get('/ajax/index/user', 'UserController@ajaxIndex')->name('user.indexAjax');
    //------------------------<User routes>----------------------------------------------------------------------------

    //------------------------<Institution routes>----------------------------------------------------------------------
    Route::resource('/dashboard/institution', 'InstitutionController');
    Route::get('/ajax/find/institution', 'InstitutionController@findAjax')->name('institution.find');
    Route::get('/ajax/delete/institution', 'InstitutionController@ajaxDelete')->name('institution.remove');
    Route::get('/ajax/index/institution', 'InstitutionController@ajaxIndex')->name('institution.indexAjax');
    Route::get('/ajax/show/institution', 'InstitutionController@indexAjaxSelect')->name('institution.indexAjaxSelect');
    //------------------------<Institution routes>----------------------------------------------------------------------

    //------------------------<Catalogue routes>----------------------------------------------------------------------
    Route::resource('/dashboard/catalogue', 'CatalogueController');
    Route::get('/ajax/delete/catalogue', 'CatalogueController@ajaxDelete')->name('catalogue.remove');
    Route::get('/ajax/index/catalogue/{valor?}', 'CatalogueController@ajaxIndex')->name('catalogue.indexAjax');
    Route::get('/ajax/show/catalogue', 'CatalogueController@indexAjaxSelect')->name('catalogue.indexAjaxSelect');
    Route::get('/ajax/select/catalogue', 'CatalogueController@findCatalogo')->name('catalogue.find');
    //------------------------<Catalogue routes>----------------------------------------------------------------------

    //------------------------<Career routes>----------------------------------------------------------------------
    Route::resource('/dashboard/career', 'CareerController');
    Route::get('/ajax/index/career/{value?}', 'CareerController@ajaxIndex')->name('career.indexAjax');
    //------------------------<Career routes>----------------------------------------------------------------------

    //------------------------<IconResource routes>----------------------------------------------------------------------
    Route::resource('/iconresource', 'IconResourceController');
    //------------------------<IconResource routes>----------------------------------------------------------------------

});

//------------------------<Landing routes>-----------------------------------------------------------------------------
Route::get('/privacy', function () {
    return view('landing.footerPages.privacy');
});
Route::get('/about', function () {
    return view('landing.footerPages.about');
});
Route::get('/terms', function () {
    return view('landing.footerPages.terms');
});
Route::get('/careers', function () {
    return view('layouts.search');
});
//------------------------<Landing routes>-----------------------------------------------------------------------------

//------------------------<Icon Resources routes>-----------------------------------------------------------------------
Route::resource('/IconResource', 'IconResourceController');
//------------------------<Icon Resources routes>-----------------------------------------------------------------------


//
//Route::get('/{vue_capture?}', function () {
//    return view('coreui');
//})->where('vue_capture', '[\/\w\.-]*');