<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 6/28/18 9:00 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

try {
    Breadcrumbs::register('home', function ($breadcrumbs) {
        $breadcrumbs->push('Dashboard   ', route('home'));
    });
    Breadcrumbs::register('enclosure.index', function ($breadcrumbs) {
        $breadcrumbs->parent('home');
        $breadcrumbs->push('recinto', route('enclosure.index'));
    });

    Breadcrumbs::register('enclosure.create', function ($breadcrumbs) {
        $breadcrumbs->parent('enclosure.index');
        $breadcrumbs->push('Nuevo recinto', route('enclosure.create'));
    });

    Breadcrumbs::register('enclosure.edit', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('enclosure.index');
        //Kavv
        //Change from $id->nombre to $id->name
        $breadcrumbs->push($id->name, route('enclosure.edit', $id));
        $breadcrumbs->push('Edit');
    });

    Breadcrumbs::register('institution.index', function ($breadcrumbs) {
        $breadcrumbs->parent('home');
        $breadcrumbs->push('institucion', route('institution.index'));
    });

    Breadcrumbs::register('institution.create', function ($breadcrumbs) {
        $breadcrumbs->parent('institution.index');
        $breadcrumbs->push('Nueva institucion', route('institution.create'));
    });

    Breadcrumbs::register('institution.edit', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('institution.index');
        $breadcrumbs->push($id->nombre, route('institution.edit', $id));
        $breadcrumbs->push('Edit');
    });

    Breadcrumbs::register('user.index', function ($breadcrumbs) {
        $breadcrumbs->parent('home');
        $breadcrumbs->push('usuario', route('user.index'));
    });

    Breadcrumbs::register('user.create', function ($breadcrumbs) {
        $breadcrumbs->parent('user.index');
        $breadcrumbs->push('Nuevo usuario', route('user.create'));
    });

    Breadcrumbs::register('user.edit', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('user.index');
        $breadcrumbs->push($id->name, route('user.edit', $id));
        $breadcrumbs->push('Edit');
    });
    Breadcrumbs::register('catalogue.index', function ($breadcrumbs) {
        $breadcrumbs->parent('home');
        $breadcrumbs->push('catalogo', route('catalogue.index'));
    });

    Breadcrumbs::register('permission.index', function ($breadcrumbs) {
        $breadcrumbs->parent('home');
        $breadcrumbs->push('Permission', route('permission.index'));
    });

    Breadcrumbs::register('role.index', function ($breadcrumbs) {
        $breadcrumbs->parent('home');
        $breadcrumbs->push('Role', route('role.index'));
    });
    
    //Kavv
    //Career

    Breadcrumbs::register('career.index', function ($breadcrumbs) {
        $breadcrumbs->parent('home');
        $breadcrumbs->push('career', route('career.index'));
    });
    Breadcrumbs::register('career.create', function ($breadcrumbs) {
        $breadcrumbs->parent('career.index');
        $breadcrumbs->push('New career', route('career.create'));
    });
    Breadcrumbs::register('career.edit', function ($breadcrumbs, $career) {
        $breadcrumbs->parent('career.index');
        $breadcrumbs->push($career->name, route('career.edit', $career));
        $breadcrumbs->push('Edit');
    });

} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {
}

