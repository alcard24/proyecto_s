<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 7/1/18 5:41 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Http\Controllers;

use App\Steps\Enclosure\Enclosure;
use App\Steps\Enclosure\Repositories\EnclosureRepositoryInterface;
use App\Steps\Enclosure\Request\EnclosureRequest;
use GeneaLabs\LaravelMaps\Facades\Map;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Yajra\Datatables\Datatables;

/**
 * Class Enclosure Controller
 * @package App\Http\Controllers
 */
class EnclosureController extends Controller
{

    /**
     * @var EnclosureRepositoryInterface
     */
    private $enclosureRepository;

    /**
     * EnclosureController constructor.
     * @param EnclosureRepositoryInterface $enclosureRepository
     */
    public function __construct(EnclosureRepositoryInterface $enclosureRepository)
    {
        $this->middleware('auth');
        $this->enclosureRepository = $enclosureRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $predicate
     * @return \Illuminate\Http\Response
     */
    public function index(Request $predicate)
    {
        return view('dashboard.enclosure.index', ['url' => 'dashboard/enclosure']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //set up default configs
        $config = array();
        $config['map_height'] = '200px';
        $config['zoom'] = 'auto';
        $config['places'] = TRUE;
        $config['draggableCursor'] = 'default';
        $config['center'] = 'auto';
        $config['placesAutocompleteInputID'] = 'mySearchPlace';
        $config['placesAutocompleteBoundsMap'] = TRUE; // set results biased towards the maps viewport
        $config['placesAutocompleteOnChange'] = ' 
        var before = markers_map.pop();
         if(before != null)
            before.setMap(null);
         markers_map.push(new google.maps.Marker({
            position: placesAutocomplete.getPlace().geometry.location, 
            map: map
        }));
        $("#latitude").val(placesAutocomplete.getPlace().geometry.location.lat()); 
        $("#longitude").val(placesAutocomplete.getPlace().geometry.location.lng());
       
        $("#latitude").parent().removeClass("is-empty");
        $("#longitude").parent().removeClass("is-empty");
        map.setCenter(placesAutocomplete.getPlace().geometry.location); 
        map.setZoom(15);
        ';
        $config['onclick'] = '
         var before = markers_map.pop();
         if(before != null)
            before.setMap(null);
         markers_map.push(new google.maps.Marker({
            position: event.latLng, 
            map: map
        }));
        
        $("#latitude").val(event.latLng.lat()); 
        $("#longitude").val(event.latLng.lng());
        $("#latitude").parent().removeClass("is-empty");
        $("#longitude").parent().removeClass("is-empty");
        
        map.setCenter(event.latLng); 
        
        map.setZoom(15);
        ';

        Map::initialize($config);

        $data = Map::create_map();

        return view('dashboard.enclosure.edit')->with('data', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param EnclosureRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(EnclosureRequest $request)
    {

        $this->enclosureRepository->create($request->all());

        //Kavv
        //Cambio de dashboard/recinto a dashboard/enclosure
        return Redirect::to('dashboard/enclosure');

    }

    /**
     * Show the form for editing the specified resource.
     *
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        /**
         * @var Enclosure
         */
        $enclosure = Enclosure::findOrFail($id);

        if (empty($enclosure)) {
            return response()->isNotFound();
        } else {
            $ins = $enclosure->getInstitution;
            $canAddLocation = !empty($enclosure->latitude) && !empty($enclosure->longitude);

            //set up default configs
            $config = array();
            $config['map_height'] = '200px';
            $config['zoom'] = '15';
            $config['places'] = TRUE;
            $config['draggableCursor'] = 'default';
            if ($canAddLocation) {

                $config['center'] = $enclosure->latitude . ',' . $enclosure->longitude;

            }
            $config['placesAutocompleteInputID'] = 'mySearchPlace';
            $config['placesAutocompleteBoundsMap'] = TRUE; // set results biased towards the maps viewport
            $config['placesAutocompleteOnChange'] = ' 
                var before = markers_map.pop();
                 if(before != null)
                    before.setMap(null);
                 markers_map.push(new google.maps.Marker({
                    position: placesAutocomplete.getPlace().geometry.location, 
                    map: map
                }));
                $("#latitud").val(placesAutocomplete.getPlace().geometry.location.lat()); 
                $("#longitud").val(placesAutocomplete.getPlace().geometry.location.lng());
               
                $("#latitud").parent().removeClass("is-empty");
                $("#longitud").parent().removeClass("is-empty");
                map.setCenter(placesAutocomplete.getPlace().geometry.location); 
                map.setZoom(15);
                ';
            $config['onclick'] = '
                 
                 var before = markers_map.pop();
                 if(before != null)
                    before.setMap(null);
                 markers_map.push(new google.maps.Marker({
                    position: event.latLng, 
                    map: map
                }));
                
                $("#latitud").val(event.latLng.lat()); 
                $("#longitud").val(event.latLng.lng());
                $("#latitud").parent().removeClass("is-empty");
                $("#longitud").parent().removeClass("is-empty");
                map.setCenter(event.latLng); 
                
                map.setZoom(15);
                ';

            Map::initialize($config);

            if ($canAddLocation) {
                $marker = array();
                $marker['position'] = $enclosure->latitude . ',' . $enclosure->longitude;
                $config['center'] = 'auto';
                Map::add_marker($marker);
            }

            $data = Map::create_map();
            //Kavv
            //$selected_ins wasn't declared and it isn't used
            $selected_ins = 1;
            return view('dashboard.enclosure.edit', ['enclosure' => $enclosure, 'institution' => $ins])->with('data', $data);
        }
    }

    /**
     * @param EnclosureRequest $request
     * @param Enclosure $enclosure
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EnclosureRequest $request, Enclosure $enclosure)
    {
        $this->enclosureRepository->update($request->all(), $enclosure->id);

        return Redirect::to('dashboard/enclosure');
    }

    /**
     * Ajax Delete, Method
     * @param Request $enclosure
     * @return void
     */
    public function ajaxDelete(Request $enclosure)
    {
        $this->enclosureRepository->delete($enclosure->get('id'));
    }

    /**
     * Ajax Index, Method GET
     * @return mixed
     * @throws \Exception
     */
    public function ajaxIndex()
    {
        $data = Enclosure::all();

        return Datatables::of($data)
            ->addColumn('action', function (Enclosure $data) {
                //Kavv
                //Cambio de recinto.edit a enclosure.edit
                return '<a href="' . route('enclosure.edit', $data->id) . '"' . '<button type="button" rel="tooltip" class="btn btn-success btn-simple"  data-original-title="" title="">' .
                    '<i class="material-icons" > edit</i></button ></a >' .
                    '<button rel="tooltip" class="btn btn-danger btn-simple remove" data-original-title="" title="" data-info="' . $data->id . '"> <i class="material-icons">close</i> </button>';
            })
            ->addColumn('created_at', function ($data) {
                return $data->created_at->diffForHumans();
            })
            ->make(true);
    }

}
