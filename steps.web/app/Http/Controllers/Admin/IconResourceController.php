<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 6/28/18 8:57 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Http\Controllers;

use App\Steps\IconResource\IconResource;
use Illuminate\Http\Request;

class IconResourceController extends Controller
{
    public function index(Request $request)
    {
        $MAX_RECORDS = 5;
        $icons = [];
        $resp = [];
        if (empty($request->get('query'))) {
            $icons = IconResource::take($MAX_RECORDS)->orderBy('name')->get();
            $resp = $this->retriveResult($icons);
            return response()->json($resp);
        }
        $icons = IconResource::where('name', 'like', '%' . $request->get('query') . '%')->take(5)->get();
        $resp =  $this->retriveResult($icons);
        return response()->json($resp);
    }

    private function retriveResult($IconArray)
    {
        $data = [];
        foreach ($IconArray as $item) {
            $data[] = ['id' => $item->id, 'text' => $item->name ];
        }
        return $data;
    }
}
