<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 7/1/18 6:24 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Steps\Career\Career;
use Validator;
/**
 * Class Career Controller
 * @package App\Http\Controllers
 */
class CareerController extends Controller
{
    /**
     * Index, Method GET
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        return view('dashboard.career.index', ['url' => 'dashboard/career']);
    }

    public function create() {
        return view('dashboard.career.edit');
    }

    public function store(Request $request) {
        //the validation gave me a lot of errors so i finally made this:
        //Request Validation Without redirect
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255|regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-]*)*)+$/',
            'career_select' => 'nullable|integer',
            'content' => 'nullable|integer'
        ]);
        //If exists error in the validation then
        if ($validator->fails()) {
            //return json with errors list and action for take a decision in the js
            return response()->json([
                'errors' => $validator->errors(),
                'action' => 1
            ],400); //with error state 400
        }

        Career::create([
            'name' => $request['name'],
            'especial_of' => $request['career_select'],
        ]);
        return response()->json(['action' => 0]);
    }

    public function edit($id)
    {
        $career = Career::findOrFail($id);
        return view('dashboard.career.edit', ['career' => $career]);
    }

    public function update(Request $request, $usuario)
    {
        //Kavv
        //Email is unique but when u make update, u must validate.. because exist or not this email
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255|regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-]*)*)+$/',
            'career_select' => 'nullable|integer',
            'content' => 'nullable|integer'
        ]);

        //If exists error in the validation then
        if ($validator->fails()) {
            //return json with errors list and action for take a decision in the js
            return response()->json([
                'errors' => $validator->errors(),
                'action' => 1
            ],400);
        }

        $career = Career::find($usuario);
        $career->name = $request->get('name');
        if($request->get('career_select'))
            $career->especial_of = $request->get('career_select');
        $career->update();

        return response()->json(['action' => 0]);

    }
    public function destroy($id)
    {
        $career = Career::find($id);
        $career->delete();
        return 0;
    }
    public function ajaxIndex($id = null, Request $x)
    {
        if($id == null)
            $data = Career::all();
        else
            $data = Career::where('id', '!=', $id)->get();
        return Datatables::of($data)
            ->addColumn('id', function (Career $data) {
                return '<span data-id="' . $data->id . '" data-nombre="' . $data->name . '"></span>';
            })
            ->addColumn('specialPosition', function (Career $data) {
                if ($data->especial_of == null) {
                    return '<span class="label label-danger">NINGUNO</span>';
                } else
                    return '<span class="label label-primary">' . $data->getMaster->name . '</span>';
            })
            ->addColumn('content', function (Career $data) {
                if ($data->table_content == null) {
                    return '<span class="label label-danger">NINGUNO</span>';
                } else
                    return '<span class="label label-primary">' . $data->table_content . '</span>';
            })
            ->addColumn('created_at', function (Career $data) {
                return $data->created_at->diffForHumans();
            })
            ->addColumn('action', function (Career $data) {
                return
                    '<a href="' . route('career.edit', $data->id) . '"' . '<button type="button" rel="tooltip" class="btn btn-success btn-simple"  data-original-title="" title="">' .
                    '<i class="material-icons" > edit</i></button ></a >' .
                    '<button rel="tooltip" class="btn btn-danger btn-simple remove" data-original-title="" title="" data-info="' . $data->id . '"> <i class="material-icons">close</i> </button>';
            })
            ->rawColumns(['id', 'specialPosition', 'content', 'action'])
            ->make(true);
    }
}
