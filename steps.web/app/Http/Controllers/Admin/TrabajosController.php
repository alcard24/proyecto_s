<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 6/28/18 8:57 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TrabajosController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
}
