<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 7/1/18 5:36 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Http\Controllers;

use App\Steps\Catalogue\Catalogue;
use App\Steps\IconResource\IconResource;
use Illuminate\Http\Request;
use Mockery\Exception;
use Validator;
use Yajra\Datatables\Datatables;

/**
 * Class  Catalogue Controller
 * @package App\Http\Controllers
 */
class CatalogueController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Index, Method GET
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('dashboard.catalogue.catalogue', ['url' => 'dashboard/catalogue']);
    }


    /**
     * Store, Method POST
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $errors = Validator::make($request->all(), [
            'codigo' => 'unique:catalogues,code|nullable|max:20',
            'nombre' => 'required|max:500',
            'descripcion' => 'nullable|max:200',
        ]);

        $errors->valid();

        if ($errors->errors()->any()) {
            $htmlErrorsRaw = '';
            foreach ($errors->errors()->all() as $error) {
                $htmlErrorsRaw .= '<li>' . $error . '</li>';
            }
            return response()->json(['message' => 'Ocurrio un error', 'htmlRaw' => '<div class="alert alert-danger"><ul>' . $htmlErrorsRaw . '</ul></div>', 'errors' => $errors->errors()], 400);
        }

        $catalog = new Catalogue();

        $catalog->name = $request->get('nombre');
        $catalog->code = $request->get('codigo');
        $catalog->description = $request->get('descripcion');
        $catalog->icon = $request->get('icon');
        $catalog->master = $request->get('catalogo_padre_select');

        if ($catalog->save()) {
            return response()->json('Guardado correctamente', 200);
        } else {
            return response()->json('Error al guardar', 400);
        }
    }

    /**
     * Update, Method PUT/PATCH
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $errors = Validator::make($request->all(),[
                'nombre' => 'required|max:500',
                'descripcion' => 'nullable|max:200',
            ]);

            $errors->valid();
            if ($errors->errors()->any()) {
                $htmlErrorsRaw = '';
                foreach ($errors as $error) {
                    $htmlErrorsRaw .= '<li>' . $error . '</li>';
                }
                return response()->json(['message' => 'Ocurrio un error', 'htmlRaw' => '<div class="alert-inverse alert-danger"><ul>' . $htmlErrorsRaw . '</ul></div>', 'errors' => $errors->all()], 400);
            }
            $catalogue = Catalogue::find($id);
            $catalogue->name = $request->get('nombre');
            $catalogue->code = $request->get('codigo');
            $catalogue->description = $request->get('descripcion');
            $catalogue->icon = $request->get('icon');
            $catalogue->master = $request->get('catalogo_padre_select');
            $catalogue->save();
            return response('Actualizado correctamente', 200);
        } catch (Exception $e) {
            return response('Error al guardar', 400);
        }
    }

    /**
     * Ajax Delete, Method GET
     * @param Request $catalog
     * @return bool
     */
    public function ajaxDelete(Request $catalog)
    {
        $catalog = Catalogue::find($catalog->input('catalogo'));
        try {
            $catalog->delete();
        } catch (\Exception $e) {
            return response()->isInvalid();
        }
        return 1;

    }
    public function ajaxIndex($value = null, Request $request)
    {
        $data = [];
        if($value == null && $request['id'] == null)
            $data = Catalogue::all();
        //Kavv
        //when we specified a value "id", it's because we are editing, so we don't need show this catalogue as father
        //this is excluded 
        if($request['id'] != null)
            $data = Catalogue::where('id','!=', $request['id'])->get();
        if($value == 'quiz')
        {
            $padre = Catalogue::where('code','TT')->first();
            if($padre)
                $data = $data->getAllChildren;
        }
        return Datatables::of($data)
            ->addColumn('id', function (Catalogue $data) {
                return '<span data-id="' . $data->id . '" data-nombre="' . $data->name . '"></span>';
            })
            ->addColumn('descripcion', function (Catalogue $data) {
                return $data->description;
            })
            ->addColumn('action', function (Catalogue $data) {
                return
                    '<button type="button" data-info= "' . $data->id . '" rel="tooltip" class="btn btn-facebook btn-simple edit" >' .
                    '<i class="material-icons">edit</i></button>' .
                    '<button rel="tooltip" class="btn btn-danger btn-simple remove" data-original-title="" title="" data-info="' . $data->id . '"> <i class="material-icons">close</i> </button>';
            })
            ->addColumn('codigo', function (Catalogue $data) {
                if ($data->code == null) {
                    return '<span class="label label-danger">NINGUNO</span>';
                } else
                    return '<span class="label label-primary">' . $data->code . '</span>';
            })
            ->addColumn('created_at', function (Catalogue $data) {
                return $data->created_at->diffForHumans();
            })
            ->rawColumns(['action', 'codigo', 'id'])
            ->make(true);
    }

    /**
     * Index Ajax Select, Method GET
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function indexAjaxSelect(Request $request)
    {
        $data = [];

        if (!empty($request->get('idSeleccionado'))) {
            $data = Catalogue::findOrFail($request->get('idSeleccionado'))->getAllChildren;
        }
        //return response()->json($data);
        return Datatables::of($data)
            //Kavv
            //Cambio del nombre de la columna de id por nombre
            ->addColumn('nombre', function (Catalogue $data) {
                return '<span data-id="' . $data->id . '" data-nombre="' . $data->name . '">' . $data->name . '</span>';
            })
            ->addColumn('codigo', function (Catalogue $data) {
                if ($data->code == null) {
                    return '<span class="label label-danger">NINGUNO</span>';
                } else {
                    return '<span class="label label-primary">' . $data->code . '</span>';
                }
            })
            ->addColumn('action', function (Catalogue $data) {
                //Kavv
                //Es necesario especificar el type="button" en cada etiqueta button porque estaran contenidas dentro de un form
                //Si no lo especificamos se ejecutara el submit
                return
                    '<button type="button" data-info= "' . $data->id . '" rel="tooltip" class="btn btn-facebook btn-simple edit" >' .
                    '<i class="material-icons">edit</i></button>' .
                    '<button type="button" rel="tooltip" class="btn btn-danger btn-simple remove" data-original-title="" title="" data-info="' . $data->id . '"> <i class="material-icons">close</i> </button>';
            })
            //Kavv
            //Agregamos la columna nombre
            ->rawColumns(['nombre','action', 'codigo'])
            ->make(true);
    }

    /**
     * Find Catalog, Method GET
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function findCatalogo(Request $request)
    {
        
        $catalogue = Catalogue::with(['getIcon','getMaster'])->findOrFail($request->id);
        //return response()->json($catalogue);
        if (!empty($catalogue)) {
            return response()->json($catalogue);
        } else{
            return response()->isNotFound();
        }
    }
}
