<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 6/28/18 8:55 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Http\Controllers;
use App\Steps\Role\Repositories\RoleRepositoryInterface;
use App\Steps\Role\Requests\CreateRoleRequest;
use App\Steps\Role\Role;
use Yajra\DataTables\Contracts\DataTable;

/**
 * Class Role Controller
 * @package App\Http\Controllers
 */
class RoleController extends Controller
{
    /**
     * @var RoleRepositoryInterface
     */
    private $rolRepository;

    /**
     * RolController constructor.
     * @param RoleRepositoryInterface $roleRepository
     */
    public function __construct(RoleRepositoryInterface $roleRepository)
    {
        $this->middleware('auth');
        $this->rolRepository = $roleRepository;
    }

    /**
     * Index function METHOD GET
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('dashboard.role_permission.rol');
    }


    /**
     * Store Role METHOD POST
     * @param CreateRoleRequest $createRoleRequest
     */
    public function store(CreateRoleRequest $createRoleRequest) {

    }
    /**
     * Retrieve All roles
     *
     * @return mixed
     */
    public function retrieveAllRoles()
    {
        return DataTable::of(
            $this->rolRepository->all()
        )
            ->make(true);
    }
}
