<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 7/1/18 5:41 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Http\Controllers;

use App\Steps\User\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Yajra\Datatables\Datatables;
use Validator;
use DB;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

       /**
     * Display a listing of the resource.
     *
     * @param Request $predicate
     * @return \Illuminate\Http\Response
     */
    public function index(Request $predicate)
    {
        return view('dashboard.user.index',['url' => 'dashboard/user']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //set up default configs
 		return view('dashboard.user.edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //the validation gave me a lot of errors so i finally made this:
        //Request Validation Without redirect
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:50|regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-]*)*)+$/',
            'email' => 'required|max:70|email',
            'password' => 'required|between:8,100'
        ]);
        //If exists error in the validation then
        if ($validator->fails()) {
            //return json with errors list and action for take a decision in the js
            return response()->json([
                'errors' => $validator->errors(),
                'action' => 1
            ],400); //with error state 400
        }
        
        //Exists an email same to $request->get('email')
        $query = User::where('email','ilike',$request->get('email'))->get();

        //if $query is greater than 0, imply than the email is in used
        if(count($query) > 0)
        {
            return response()->json([
                'message' => ['El email esta en uso'],
                'action' => 1
            ]);
        }
        //if exist a deleted user with the same email
        $query = User::withTrashed()
                 ->Where('email','ilike',$request->get('email'))
                 ->get();

        //if $query is greater than 0, imply than the user will be restore
        if(count($query) > 0)
        {
            //restore
            $query[0]->deleted_at = null;
            $query[0]->save();
        }
        else
        {
            //create a new user
            $user = new User();
            $user->name = $request->get('name');
            $user->email = $request->get('email');
            $user->password = bcrypt($request->get('password'));
            $user->save();
        }

        return response()->json(['action' => 0]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Enclosure $recinto
     * @return \Illuminate\Http\Response
     */
    public function show(User $usuario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = User::findOrFail($id);
        return view('dashboard.user.edit', ['usuario' => $usuario]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $usuario
     * @return \Illuminate\Http\RedirectResponse
     */
    
    public function update(Request $request, $usuario)
    {
        
        //Kavv
        //Email is unique but when u make update, u must validate.. because exist or not this email
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:50|regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-]*)*)+$/',
            'email' => 'required|max:70|email',
            'password' => 'nullable|between:8,100'
        ]);

        //If exists error in the validation then
        if ($validator->fails()) {
            //return json with errors list and action for take a decision in the js
            return response()->json([
                'errors' => $validator->errors(),
                'action' => 1
            ],400);
        }
        $user = User::find($usuario);
        //if the email is the same, not is necessary validate the email
        if($user['email']!=$request->get('email'))
        {
            //query if exists the email
            $consulta = User::where('email','ilike',$request->get('email'))->get();
            if(count($consulta)>0)
            {
                return response()->json([
                    'message' => ['El correo ingresado ya esta en uso'],
                    'action' => 1,
                ]);
            }
            else//if the mail does not exist it can be updated
            {
                $user['email'] = $request->get('email');
            }
        }
        //Changue the password
        if($request->get('changePassword')==1)
        {
            $user['password'] = bcrypt($request->get('password'));
        }

        $user->name = $request->get('name');
        $user->update();

        return response()->json([
            'action' => 0
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($user)
    {
        $user = User::find($user);
        $user->activo = false;
        $user->update();
        return Redirect::to('dashboard/user');

    }

    public function ajaxDelete(Request $request)
    {
        //Kavv
        //Add deleting
        $user = User::find($request['user']);
        $user->delete();
    }

    public function ajaxIndex()
    {
        $data = User::all();
        $htmlDATA = '';
        return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return '<a href="' . route('user.edit', $data->id) . '"' . '<button type="button" rel="tooltip" class="btn btn-success btn-simple"  data-original-title="" title="">' .
                    '<i class="material-icons" > edit</i></button ></a >' .
                    '<button rel="tooltip" class="btn btn-danger btn-simple remove" data-original-title="" title="" data-info="' . $data->id . '"> <i class="material-icons">close</i> </button>';
            })
            ->addColumn('roles',function ($data){
                $htmlDATA = '';
                foreach ($data->roles as $rol) {
                    $htmlDATA .= '<span class="label label-primary">'.$rol->display_name.'</span>';
                }
                return $htmlDATA;
            })
            ->addColumn('created_at', function ($data) {
                return $data->created_at->diffForHumans();
            })
            ->rawColumns(['roles','action'])
            ->make(true);
    }

}
