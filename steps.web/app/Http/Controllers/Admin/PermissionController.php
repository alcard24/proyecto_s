<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 6/28/18 8:55 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Http\Controllers;

use App\Steps\Base\ActionType;
use App\Steps\Permission\Permission;
use App\Steps\Permission\Repositories\PermissionRepositoryInterface;
use App\Steps\Permission\Requests\CreatePermissionRequest;
use Illuminate\Support\Facades\Redirect;
use Yajra\Datatables\Datatables;


/**
 * Class Permission Controller
 * @package App\Http\Controllers
 */
class PermissionController extends Controller
{
    /**
     * @var PermissionRepositoryInterface
     */
    private $permissionRepository;

    /**
     * Permission Controller constructor.
     * @param PermissionRepositoryInterface $permissionRepository
     */
    public function __construct(PermissionRepositoryInterface $permissionRepository)
    {
        $this->middleware('auth');
        $this->permissionRepository = $permissionRepository;
    }

    /**
     * Index Method GET
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('dashboard.role_permission.permission');
    }

    /**
     * Store Permission
     * @param CreatePermissionRequest $createPermissionRequest
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreatePermissionRequest $createPermissionRequest)
    {
        $this->permissionRepository->create($createPermissionRequest->all());
        //Kavv
        //Add redirect
        return Redirect::to('dashboard/permission');

//        return response()->isSuccessful();
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function indexAjax()
    {
        return DataTables::of($this->permissionRepository->all())
            ->addColumn('created_at', function (Permission $permission) {
                return $permission->created_at->diffForHumans();
            })
            //Kavv
            //Add column action
            //Missing edition and deletion of permissions

            ->addColumn('action', function (Permission $permission) {
                return '<a href="#"' . '<button type="button" rel="tooltip" class="btn btn-success btn-simple"  data-original-title="" title="">' .
                    '<i class="material-icons" > edit</i></button ></a >' .
                    '<button rel="tooltip" class="btn btn-danger btn-simple remove" data-original-title="" title="" data-info="' . $permission->id . '"> <i class="material-icons">close</i> </button>';
            })
//            ->addColumn('action', function (Permission $permission) {
//                return Utils::createAction('EDIT', $permission->id, true)
//                    . Utils::createAction('DELETE', $permission->id, true);
//            })
//            ->rawColumns(['action'])
            ->make(true);
    }
}
