<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 7/1/18 5:39 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Http\Controllers;

use App\Steps\Institution\Institution;
use App\Steps\Institution\Repositories\InstitutionRepositoryInterface;
use App\Steps\Institution\Requests\InstitutionDeleteRequest;
use App\Steps\Institution\Requests\InstitutionRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\DataTables;

/**
 * Class Institution Controller
 * @package App\Http\Controllers
 */
class InstitutionController extends Controller
{

    /**
     * @var InstitutionRepositoryInterface
     */
    private $institutionRepository;

    /**
     * InstitutionController constructor.
     * @param InstitutionRepositoryInterface $institutionRepository
     */
    public function __construct(InstitutionRepositoryInterface $institutionRepository)
    {
        $this->middleware('auth');
        $this->institutionRepository = $institutionRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.institution.index', ['url' => 'dashboard/institution']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.institution.edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param InstitutionRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(InstitutionRequest $request)
    {
        $this->institutionRepository->create($request->all());

        return Redirect::to('dashboard/institution');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $institutionId
     * @return \Illuminate\Http\Response
     */
    public function edit(int $institutionId)
    {
        $ins = Institution::findOrFail($institutionId);

        return view('dashboard.institution.edit', ['institution' => $ins]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param InstitutionRequest $request
     * @param int $institutionId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(InstitutionRequest $request, int $institutionId)
    {

        $this->institutionRepository->update($request->all(), $institutionId);

        return Redirect::to('dashboard/institution');

    }

    /**
     * Ajax Delete
     * @param InstitutionDeleteRequest $institution
     * @return \Illuminate\Http\RedirectResponse
     */
    public function ajaxDelete(InstitutionDeleteRequest $institution)
    {
        $this->institutionRepository->delete($institution->get('id'));
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function ajaxIndex()
    {
        $data = Institution::all();
        return Datatables::of($data)
            ->addColumn('action', function (Institution $data) {
                return '<a href="' . route('institution.edit', $data->id) . '"' . '<button type="button" rel="tooltip" class="btn btn-success btn-simple"  data-original-title="" title="">' .
                    '<i class="material-icons" > edit</i></button ></a >' .
                    '<button rel="tooltip" class="btn btn-danger btn-simple remove" data-original-title="" title="" data-info="' . $data->id . '"> <i class="material-icons">close</i> </button>';
            })
            ->addColumn('created_at', function ($data) {
                return $data->created_at->diffForHumans();
            })
            ->make(true);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function indexAjaxSelect()
    {
        $data = Institution::all();
        return Datatables::of($data)
            ->addColumn('id', function (Institution $data) {
                return '<span data-id="' . $data->id . '" data-nombre="' . $data->name . '"></span>';
            })
            ->addColumn('created_at', function (Institution $data) {
                return $data->created_at->diffForHumans();
            })
            ->rawColumns(['id'])
            ->make();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function findAjax(Request $request)
    {
        $term = $request->get('query');
        if (empty($term))
            return response()->json([]);


        $ins = Institution::where('name', 'like', '%' . $term . '%')->take(5)->get();
        $response = [];

        foreach ($ins as $in) {
            $response[] = ['id' => $in->id, 'text' => $in->name];
        }
        return response()->json($response);
    }
}
