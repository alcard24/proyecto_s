<?php

/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 7/15/18 11:00 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Providers;

use App\Steps\Enclosure\Repositories\EnclosureRepository;
use App\Steps\Enclosure\Repositories\EnclosureRepositoryInterface;
use App\Steps\Institution\Repositories\InstitutionRepository;
use App\Steps\Institution\Repositories\InstitutionRepositoryInterface;
use App\Steps\Permission\Repositories\PermissionRepository;
use App\Steps\Permission\Repositories\PermissionRepositoryInterface;
use App\Steps\Role\Repositories\RoleRepository;
use App\Steps\Role\Repositories\RoleRepositoryInterface;
use Illuminate\Support\ServiceProvider;

/**
 * Class Repository Service Provider
 * @package App\Providers
 */
class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        /**
         * Permission Repository
         */
        $this->app->bind(
            PermissionRepositoryInterface::class,
            PermissionRepository::class
        );

        /**
         * Role Repository
         */
        $this->app->bind(
            RoleRepositoryInterface::class,
            RoleRepository::class
        );

        /**
         * Institution Repository
         */
        $this->app->bind(
            InstitutionRepositoryInterface::class,
            InstitutionRepository::class
        );


        /**
         * Enclosure Repository
         */
        $this->app->bind(
            EnclosureRepositoryInterface::class,
            EnclosureRepository::class
        );

    }
}
