<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 7/1/18 5:54 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Steps\QuestionQuiz;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Steps\QuestionQuiz\QuestionQuiz
 *
 * @property int $id
 * @property string $header Encabezado de la pregunta.
 * @property string $body Cuerpo de la pregunta.
 * @property int $sequence_number Orden en el que se mostraran las preguntas.
 * @property int $quiz_master La relación al test.
 * @property \Carbon\Carbon|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Steps\QuestionQuiz\QuestionQuiz onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\QuestionQuiz\QuestionQuiz whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\QuestionQuiz\QuestionQuiz whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\QuestionQuiz\QuestionQuiz whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\QuestionQuiz\QuestionQuiz whereHeader($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\QuestionQuiz\QuestionQuiz whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\QuestionQuiz\QuestionQuiz whereOrderBySequence($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\QuestionQuiz\QuestionQuiz whereQuizMaster($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\QuestionQuiz\QuestionQuiz whereSequenceNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\QuestionQuiz\QuestionQuiz whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Steps\QuestionQuiz\QuestionQuiz withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Steps\QuestionQuiz\QuestionQuiz withoutTrashed()
 * @mixin \Eloquent
 */
class QuestionQuiz extends Model
{
    use SoftDeletes;
    //
    protected $dates = ['deleted_at'];
}
