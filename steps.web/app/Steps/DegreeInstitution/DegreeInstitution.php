<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 7/1/18 5:40 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Steps\DegreeInstitution;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Steps\DegreeInstitution\DegreeInstitution
 *
 * @property int $id
 * @property int $degree
 * @property int|null $content infromacion extra, de la carrera segun el colegio/ instituto. Ejemplo. el plan de clases
 * @property int $institution
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\DegreeInstitution\DegreeInstitution whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\DegreeInstitution\DegreeInstitution whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\DegreeInstitution\DegreeInstitution whereDegree($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\DegreeInstitution\DegreeInstitution whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\DegreeInstitution\DegreeInstitution whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\DegreeInstitution\DegreeInstitution whereInstitution($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\DegreeInstitution\DegreeInstitution whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class DegreeInstitution extends Model
{
    //
}
