<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 7/1/18 6:17 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Steps\InstitutionManager;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Steps\InstitutionManager\InstitutionManager
 *
 * @property int $id
 * @property int $user
 * @property int $institution
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\InstitutionManager\InstitutionManager whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\InstitutionManager\InstitutionManager whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\InstitutionManager\InstitutionManager whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\InstitutionManager\InstitutionManager whereInstitution($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\InstitutionManager\InstitutionManager whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\InstitutionManager\InstitutionManager whereUser($value)
 * @mixin \Eloquent
 */
class InstitutionManager extends Model
{
    //
}
