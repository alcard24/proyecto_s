<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 7/1/18 5:44 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Steps\ContentParent;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Steps\ContentParent\ContentParent
 *
 * @property int $id
 * @property string $name Titulo de la tabla de contenido.
 * @property string|null $description Descripcion de la tabla de contenido.
 * @property bool $display_before_default Renderizar después de la  tabla de contenido por defecto.
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\ContentParent\ContentParent whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\ContentParent\ContentParent whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\ContentParent\ContentParent whereDisplayBeforeDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\ContentParent\ContentParent whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\ContentParent\ContentParent whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\ContentParent\ContentParent whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ContentParent extends Model
{
    //
}
