<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 7/16/18 12:22 AM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Steps\Institution\Requests;
use App\Steps\Base\BaseFormRequest;

/**
 * Class Create Institution Request
 * @package App\Steps\Institution\Requests
 */
class InstitutionRequest extends BaseFormRequest
{

    /**
     * Rules
     * @return array
     */
    public function rules() {
        return [
            'name' => 'required|max:50|regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-]*)*)+$/',
            'web' => 'max:150|url|active_url|nullable',
            'mission' => 'max:250',
            'vision' => 'max:250'
        ];
    }
}