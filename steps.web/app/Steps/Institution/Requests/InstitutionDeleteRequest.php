<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 11/16/18 2:55 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Steps\Institution\Requests;

use App\Steps\Base\BaseFormRequest;

class InstitutionDeleteRequest extends BaseFormRequest
{

    /**
     *
     */
    function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}