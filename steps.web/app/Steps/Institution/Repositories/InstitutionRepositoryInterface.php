<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 7/16/18 12:25 AM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Steps\Institution\Repositories;

use App\Steps\Base\Interfaces\BaseRepositoryInterface;

/**
 * Interface Institution Repository Interface
 * @package App\Steps\Institution\Repositories
 */
interface InstitutionRepositoryInterface extends BaseRepositoryInterface
{
}