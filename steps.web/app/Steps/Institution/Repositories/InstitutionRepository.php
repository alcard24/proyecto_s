<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 7/16/18 12:58 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Steps\Institution\Repositories;

use App\Steps\Base\BaseRepository;
use App\Steps\Institution\Institution;

/**
 * Class Institution Repository
 * @package App\Steps\Institution\Repositories
 */
class InstitutionRepository extends BaseRepository implements InstitutionRepositoryInterface
{
    /**
     * @var Institution
     */
    protected $institution;

    /**
     * Institution Repository constructor.
     * @param Institution $institution
     */
    public function __construct(Institution $institution)
    {
        parent::__construct($institution);
        $this->institution = $institution;
    }

}