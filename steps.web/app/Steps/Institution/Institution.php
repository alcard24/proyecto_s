<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 7/1/18 5:39 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Steps\Institution;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Steps\Institution\Institution
 *
 * @property int $id
 * @property string $name
 * @property string|null $web_page
 * @property string|null $path_logo
 * @property string|null $path_background
 * @property string|null $mission
 * @property string|null $vision
 * @property \Carbon\Carbon|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Steps\Institution\Institution onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Institution\Institution whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Institution\Institution whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Institution\Institution whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Institution\Institution whereMission($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Institution\Institution whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Institution\Institution wherePathBackground($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Institution\Institution wherePathLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Institution\Institution whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Institution\Institution whereVision($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Institution\Institution whereWebPage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Steps\Institution\Institution withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Steps\Institution\Institution withoutTrashed()
 * @mixin \Eloquent
 */
class Institution extends Model
{
    use SoftDeletes;

    protected $guarded = [];
    protected $primaryKey = 'id';
    protected $table = 'institutions';

    protected $fillable = [
        'name',
        'mission',
        'vision',
        'web_page'
    ];


    protected $dates = ['deleted_at'];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($institution) {
            //Kavv
            //Si existe alguna relacion con el registro a eliminarse procedera a elimanar dichas relaciones
            if(count($institution->getAllEnclosures)>0)
                $institution->getAllEnclosures->delete();
        });
    }

    /**
     * Get All Enclosures
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getAllEnclosures()
    {
        //Kavv
        //Cambio de App\Enclosure por App\Steps\Enclosure\Enclosure
        return $this->hasMany('App\Steps\Enclosure\Enclosure', 'institution', 'id');
    }

}
