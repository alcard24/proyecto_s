<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 7/1/18 5:43 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Steps\Career;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Steps\Career\Career
 *
 * @property int $id
 * @property string $name
 * @property int|null $especial_of Relación para identificarlo como una especialidad de una carrera.
 * @property int|null $table_content Relación para generar la tabla de contenidos, que describe la carrera.
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Career\Career whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Career\Career whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Career\Career whereEspecialOf($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Career\Career whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Career\Career whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Career\Career whereTableContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Career\Career whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Career extends Model
{
    use SoftDeletes;

    protected $table = 'careers';
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name', 'especial_of', 'table_content'
    ];

    public function getAllChildren()
    {
        return $this->hasMany('App\Steps\Career\Career', 'especial_of', 'id');
    }

    public function getMaster(){
        return $this->belongsTo('App\Steps\Career\Career','especial_of','id');
    }
}
