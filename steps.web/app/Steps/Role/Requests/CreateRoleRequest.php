<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 7/15/18 10:30 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Steps\Role\Requests;

use App\Steps\Base\BaseFormRequest;


/**
 * Class Create Role Request
 * @package App\Steps\Role\Requests
 */
class CreateRoleRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:roles,name',
            'display_name' => 'nullable|max:200',
            'description' => 'nullable|max:500'
        ];
    }
}