<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 6/28/18 8:59 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Steps\Role;

use Zizaco\Entrust\EntrustRole;

/**
 *
 * @property int $id
 * @property string $name Name of role
 * @property string $display_name Name to display
 * @property string $description Description of Role
 * Class Role
 * @package App\Steps\Role
 */
class Role extends EntrustRole
{
}
