<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 7/15/18 10:21 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */


namespace App\Steps\Role\Repositories;


use App\Steps\Base\Interfaces\BaseRepositoryInterface;

/**
 * Interface Role Repository Interface
 * @package App\Steps\Role\Repositories
 */
interface RoleRepositoryInterface extends BaseRepositoryInterface
{

}