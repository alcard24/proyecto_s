<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 7/15/18 10:21 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Steps\Role\Repositories;


use App\Steps\Base\BaseRepository;
use App\Steps\Role\Role;

/**
 * Class Role Repository
 * @package App\Steps\Role\Repositories
 */
class RoleRepository extends BaseRepository implements RoleRepositoryInterface
{
    /**
     * @var Role
     */
    protected $model;

    /**
     * Role Repository constructor.
     * @param Role $role
     */
    public function __construct(Role $role)
    {
        parent::__construct($role);
        $this->model = $role;
    }
}