<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 7/1/18 5:39 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Steps\Degree;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Steps\Degree\Degree
 *
 * @property int $id
 * @property string $name Nombre de la carrera.
 * @property string|null $description
 * @property int $title_won Titulo ganado "Bachiller, Ingeniero, etc..."
 * @property int|null $min_education_level Educacion minima requerida, para estudiar el titulo
 * @property float|null $time_to_complete Tiempo de duración del estudio.
 * @property int|null $time_measure Medida de tiempo
 * @property float|null $time_in_years Tiempo en años para poder hacer filtros.
 * @property int|null $contents Puede añadir otro contenido, para describir mejor. la carrera
 * @property int|null $faculty Facultad, o tipo de estudio ejemplo "Ciencias exactas.".
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Degree\Degree whereContents($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Degree\Degree whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Degree\Degree whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Degree\Degree whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Degree\Degree whereFaculty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Degree\Degree whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Degree\Degree whereMinEducationLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Degree\Degree whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Degree\Degree whereTimeInYears($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Degree\Degree whereTimeMeasure($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Degree\Degree whereTimeToComplete($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Degree\Degree whereTitleWon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Degree\Degree whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Degree extends Model
{
    //
}
