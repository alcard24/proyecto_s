<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 7/15/18 11:11 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Steps\Permission\Repositories;
use App\Steps\Base\Interfaces\BaseRepositoryInterface;

/**
 * Interface Permission RepositoryInterface
 * @package App\Steps\Permission\Repositories
 */
interface PermissionRepositoryInterface extends BaseRepositoryInterface
{
    
}