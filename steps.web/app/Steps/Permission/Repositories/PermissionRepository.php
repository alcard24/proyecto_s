<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 7/15/18 11:11 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Steps\Permission\Repositories;

use App\Steps\Base\BaseRepository;
use App\Steps\Permission\Permission;

/**
 * Class Permission Repository
 * @package App\Steps\Permission\Repositories
 */
class PermissionRepository extends BaseRepository implements PermissionRepositoryInterface
{
    /**
     * @var Permission
     */
    protected $model;

    /**
     * Permission Repository constructor.
     * @param Permission $permission
     */
    public function __construct(Permission $permission)
    {
        parent::__construct($permission);
        $this->model = $permission;
    }

}