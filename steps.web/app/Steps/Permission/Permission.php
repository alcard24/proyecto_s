<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 7/15/18 9:23 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Steps\Permission;


use Zizaco\Entrust\EntrustPermission;


/**
 * App\Steps\Permission\Permission
 *
 * @property int $id
 * @property string $name
 * @property string|null $display_name
 * @property string|null $description
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Steps\Role\Role[] $roles
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Permission\Permission whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Permission\Permission whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Permission\Permission whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Permission\Permission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Permission\Permission whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Permission\Permission whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Permission extends EntrustPermission
{

    protected $table = 'permissions';
    protected $primaryKey = 'id';

    protected $fillable = [
        'name',
        'display_name',
        'description'
    ];
}
