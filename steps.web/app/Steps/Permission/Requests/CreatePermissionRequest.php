<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 7/15/18 11:15 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Steps\Permission\Requests;

use App\Steps\Base\BaseFormRequest;

/**
 * Class Create Permission Request
 * @package App\Steps\Permission\Requests
 */
class CreatePermissionRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'name' => 'required|unique:permissions,name',
            'display_name' => 'nullable|max:50',
            'description' => 'nullable|max:500'
        ];
    }
}