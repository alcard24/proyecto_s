<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 11/16/18 2:12 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Steps\Enclosure\Repositories;

use App\Steps\Base\BaseRepository;
use App\Steps\Enclosure\Enclosure;

/**
 * Class EnclosureRepository
 * @package App\Steps\Enclosure\Repositories
 */
class EnclosureRepository extends BaseRepository implements EnclosureRepositoryInterface
{
    /**
     * @var Enclosure
     */
    protected $enclosure;

    /**
     * Enclosure Repository constructor.
     * @param Enclosure $enclosure
     */
    public function __construct(Enclosure $enclosure)
    {
        parent::__construct($enclosure);
        $this->$enclosure = $enclosure;
    }
}