<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 11/16/18 2:12 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Steps\Enclosure\Repositories;

use App\Steps\Base\Interfaces\{
    BaseRepositoryInterface
};

/**
 * Interface EnclosureRepositoryInterface
 * @package App\Steps\Enclosure\Repositories
 */
interface EnclosureRepositoryInterface extends BaseRepositoryInterface
{


}