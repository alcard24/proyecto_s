<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 7/1/18 5:41 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Steps\Enclosure;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * App\Steps\Enclosure\Enclosure
 *
 * @property int $id
 * @property string $name
 * @property string $address
 * @property float $latitude
 * @property float $longitude
 * @property string|null $web
 * @property int|null $institution
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Steps\Enclosure\Enclosure onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Enclosure\Enclosure whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Enclosure\Enclosure whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Enclosure\Enclosure whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Enclosure\Enclosure whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Enclosure\Enclosure whereInstitution($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Enclosure\Enclosure whereLatitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Enclosure\Enclosure whereLongitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Enclosure\Enclosure whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Enclosure\Enclosure whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Enclosure\Enclosure whereWeb($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Steps\Enclosure\Enclosure withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Steps\Enclosure\Enclosure withoutTrashed()
 * @mixin \Eloquent
 */
class Enclosure extends Model
{
    use SoftDeletes;


    protected $table = 'enclosures';
    protected $guarded = [];
    protected $primaryKey = 'id';
    protected $fillable = [
        'name', 'address', 'latitude', 'longitude', 'web', 'institution'
    ];


    protected $dates = ['deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getInstitution()
    {
        //Kavv
        //Cambio de App\Institution por App\Steps\Institution\Institution
        return $this->belongsTo('App\Steps\Institution\Institution', 'institution', 'id'); // Detalle a maestro
    }
}
