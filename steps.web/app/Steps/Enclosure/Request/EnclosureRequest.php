<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 11/16/18 2:18 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Steps\Enclosure\Request;

use App\Steps\Base\BaseFormRequest;

class EnclosureRequest extends BaseFormRequest
{
    /**
     * Rules Enclosure request
     * @return mixed
     */
    public function rules()
    {
        return [
            'name' => 'required|max:50|regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-]*)*)+$/',
            'address' => 'required|max:70|nullable',
            'web' => 'required|max:150|url|active_url',
            'institution' => 'required',
            'latitude' => 'max:180|numeric|nullable|required_with:longitude',
            'longitude' => 'max:180|numeric|nullable'
        ];
    }
}