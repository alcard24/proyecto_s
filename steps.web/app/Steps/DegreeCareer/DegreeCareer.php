<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 7/1/18 5:45 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Steps\DegreeCareer;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Steps\DegreeCareer\DegreeCareer
 *
 * @property int $id
 * @property int $career
 * @property int $degree Tabla puente
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\DegreeCareer\DegreeCareer whereCareer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\DegreeCareer\DegreeCareer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\DegreeCareer\DegreeCareer whereDegree($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\DegreeCareer\DegreeCareer whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\DegreeCareer\DegreeCareer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\DegreeCareer\DegreeCareer whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class DegreeCareer extends Model
{
    //
}
