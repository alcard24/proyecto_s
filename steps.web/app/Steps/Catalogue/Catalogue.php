<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 7/1/18 5:36 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Steps\Catalogue;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Steps\Catalogue\Catalogue
 *
 * @property int $id
 * @property string|null $code
 * @property string $name
 * @property string|null $description
 * @property int|null $master Relación inclusiva, puede tener hijos
 * @property int|null $icon Poder relacionar un icono al catalogo
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Steps\Catalogue\Catalogue onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Catalogue\Catalogue whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Catalogue\Catalogue whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Catalogue\Catalogue whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Catalogue\Catalogue whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Catalogue\Catalogue whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Catalogue\Catalogue whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Catalogue\Catalogue whereMaster($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Catalogue\Catalogue whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Catalogue\Catalogue whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Steps\Catalogue\Catalogue withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Steps\Catalogue\Catalogue withoutTrashed()
 * @mixin \Eloquent
 */
class Catalogue extends Model
{
    use SoftDeletes;

    protected $table = 'catalogues';
    protected $dates = ['deleted_at'];

    public function getIcon()
    {
        return $this->belongsTo('App\Steps\IconResource\IconResource', 'icon', 'id');
    }

    public function getAllChildren()
    {
        return $this->hasMany('App\Steps\Catalogue\Catalogue', 'master', 'id');
    }

    public function getMaster(){
        return $this->belongsTo('App\Steps\Catalogue\Catalogue','master','id');
    }

    protected static function boot() {
        parent::boot();

        static::deleting(function($catalogo) {
            //Kavv
            //Cambio de getAllChildrens() a getAllChildren()
            foreach ($catalogo->getAllChildren()->get() as $ct) {
                $ct->delete();
            }
        });
    }
}
