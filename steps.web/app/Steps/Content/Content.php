<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 7/1/18 5:44 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Steps\Content;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Steps\Content\Content
 *
 * @property int $id
 * @property string $header
 * @property string $body Codigo html, permite describir el contenido.
 * @property int $content_master Relación, con la tabla de contenido.
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Content\Content whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Content\Content whereContentMaster($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Content\Content whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Content\Content whereHeader($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Content\Content whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Content\Content whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Content extends Model
{
    //
}
