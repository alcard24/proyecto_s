<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 7/1/18 6:14 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Steps\IconResource;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Steps\IconResource\IconResource
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\IconResource\IconResource whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\IconResource\IconResource whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\IconResource\IconResource whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\IconResource\IconResource whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\IconResource\IconResource whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class IconResource extends Model
{
    protected $guarded = [];
    protected $primaryKey = 'id';
    protected $table = 'icon_resources';
    protected $fillable = [
        'id'
    ];
}
