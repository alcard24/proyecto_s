<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 7/1/18 6:08 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Steps\Quiz;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * App\Steps\Quiz\Quiz
 *
 * @property int $id
 * @property string $name Nombre del test.
 * @property string $propose Proposito del test
 * @property int $test_type Tipo de test ("Respuesta unica, multi respuesta, mixto")
 * @property int $author
 * @property bool $order_by_sequence Comportamiento, donde las preguntas se ordenaran según el número de secuencia.
 * @property bool $shared Compartido, con la comunidad de info-edu para poder ser realizado en cualquier momento
 * @property \Carbon\Carbon|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Steps\Quiz\Quiz onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Quiz\Quiz whereAuthor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Quiz\Quiz whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Quiz\Quiz whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Quiz\Quiz whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Quiz\Quiz whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Quiz\Quiz wherePropose($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Quiz\Quiz whereShared($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Quiz\Quiz whereTestType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\Quiz\Quiz whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Steps\Quiz\Quiz withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Steps\Quiz\Quiz withoutTrashed()
 * @mixin \Eloquent
 */
class Quiz extends Model
{

    use SoftDeletes;
    //
    protected $dates = ['deleted_at'];
}
