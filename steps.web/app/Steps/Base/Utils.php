<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 7/23/18 2:05 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Steps\Base;

use phpDocumentor\Reflection\Types\Integer;

/**
 * Class Utils
 * @package App\Steps\Base
 */
class Utils
{
    /**
     * Create Action
     * @param string $actionType
     * @param int $identification
     * @param bool $isAjax
     * @return string
     */
    public static function createAction(string $actionType,
                                        int $identification,
                                        bool $isAjax)
    {

        $htmlRaw = '';

        switch (strtoupper($actionType)) {
            case 'EDIT':
                $htmlRaw =  '<button type="button" data-info= "' . $identification . '" rel="tooltip" title="Edit" class="btn btn-facebook btn-simple edit" >' .
                    '<i class="material-icons">edit</i></button>';

                break;
            case 'DELETE':
                $htmlRaw =  '<button type="button" data-info= "' . $identification . '" rel="tooltip" title="Delete" class="btn btn-danger btn-simple remove" >' .
                    '<i class="material-icons">close</i></button>';

                break;
            case 'SHOW':
                $htmlRaw =  '<button type="button" data-info= "' . $identification . '" rel="tooltip" title="Show" class="btn btn-default btn-simple show" >' .
                    '<i class="material-icons">remove_red_eye</i></button>';

                break;
        }

        return $htmlRaw;
    }

    /**
     * Initalize, Self Static
     */
    private static function initialize()
    {

    }

}