<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 7/15/18 9:41 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Steps\User;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Steps\User\User
 *
 * @property int $id
 * @property string $name
 * @property string $avatar
 * @property string $email
 * @property string $password
 * @property string|null $deleted_at
 * @property string|null $remember_token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Steps\Role\Role[] $roles
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\User\User whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\User\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\User\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\User\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\User\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\User\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\User\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\User\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\User\User whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes { restore as private restoreA; }
    use EntrustUserTrait { restore as private restoreB; }
    public function restore()
    {
        $this->restoreA();
        $this->restoreB();
    }
    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $dates = ['deleted_at'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


}
