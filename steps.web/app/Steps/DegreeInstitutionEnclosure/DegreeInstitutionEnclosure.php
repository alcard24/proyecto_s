<?php
/**
 * @copyright Copyright (c) 2017 - 2018.
 * @author Kevin Alexander Gaitan Arguello
 * @email kevinnica02@hotmail.com
 * @date 7/1/18 5:40 PM
 * @portfolio https://gitlab.com/alcard24
 *
 */

namespace App\Steps\DegreeInstitutionEnclosure;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Steps\DegreeInstitutionEnclosure\DegreeInstitutionEnclosure
 *
 * @property int $id
 * @property int $degree_institution En que recintos se encuentra disponible esta carrera
 * @property int $enclosure enclosure
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\DegreeInstitutionEnclosure\DegreeInstitutionEnclosure whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\DegreeInstitutionEnclosure\DegreeInstitutionEnclosure whereDegreeInstitution($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\DegreeInstitutionEnclosure\DegreeInstitutionEnclosure whereEnclosure($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\DegreeInstitutionEnclosure\DegreeInstitutionEnclosure whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Steps\DegreeInstitutionEnclosure\DegreeInstitutionEnclosure whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class DegreeInstitutionEnclosure extends Model
{
    //
}
